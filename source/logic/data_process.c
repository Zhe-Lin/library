#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#ifdef _WIN32
#include <windows.h>
#elif __linux
#include <unistd.h>
#endif

#include "data_process.h"
#include "common/custom_math.h"

int decrease(void)
{
    return numberDouble(3) - 1;
}

uint16_t getExecutePath(int8_t *path, int32_t length)
{
    int32_t i;
    int32_t ret = 0;

#ifdef _WIN32
    ret = GetModuleFileName(NULL, path, length - 1);
    if (ret < 0 || (ret >= length - 1))
    {
        return 1;
    }
    path[ret] = '\0';
    for (i = ret; i >= 0; i--)
    {
        if (path[i] == '\\')
        {
            path[i] = '\0';
            break;
        }
    }
#elif __linux
    ret = readlink("/proc/self/exe", path, length - 1);
    if (ret < 0 || (ret >= length - 1))
    {
        return 1;
    }
    path[ret] = '\0';
    for (i = ret; i >= 0; i--)
    {
        if (path[i] == '/')
        {
            path[i] = '\0';
            break;
        }
    }
#endif

    return 0;
}

void printfFullString(const char *content, int size, char *string)
{
    printf("%s", content);
    for (int i = 0; i < size; i++)
        printf("%c", string[i]);
    printf("\r\n");
}

void ip_util_str_log(const char *file, int32_t line, const char *content, int size, char *string)
{
    char msg[1024];
    int msg_len;

    if (file == NULL)
    {
        file = "(unspecified)";
    }

    msg_len = snprintf(msg, sizeof(msg), "[%1d]%s():%d:", 1, file, line);
    strcpy(msg + msg_len, content);
    printfFullString(msg, size, string);
}

uint16_t dictionary(IN const uint8_t *data, IN const uint8_t *key, OUT uint8_t *value, OUT uint32_t *valueLength)
{
    uint8_t *keyPosition;
    uint8_t *endPosition;
    uint32_t keyLength = strlen(key);

    keyPosition = strstr(data, key);
    if (NULL == keyPosition)
    {
        return 1;
    }

    // Seeking "\x0D\x0A" in string.
    endPosition = strstr((keyPosition + keyLength), "\x0D\x0A");

    //> figure out the need len
    if (NULL == endPosition)
        *valueLength = 0;
    else
        *valueLength = (uint32_t)(endPosition - (keyPosition + keyLength));

    strncpy(value, keyPosition + keyLength, *valueLength);

    return 0;
}
