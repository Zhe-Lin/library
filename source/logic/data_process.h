#ifndef _DATA_PROCESS_H
#define _DATA_PROCESS_H

#include <stdint.h>

#include "common/custom_types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int decrease(void);
uint16_t getExecutePath(int8_t *path, int32_t length);
void printfFullString(const char *content, int size, char *string);
void ip_util_str_log(const char *file, int32_t line, const char *content, int size, char *string);
uint16_t dictionary(IN const uint8_t *data, IN const uint8_t *key, OUT uint8_t *value, OUT uint32_t *valueLength);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _DATA_PROCESS_H