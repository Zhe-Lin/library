#ifndef _TRANSFORM_ASCII_H
#define _TRANSFORM_ASCII_H

#include "common/custom_types.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    char charToHex(const char *src);
    unsigned char charByteToHexByte(char *src);
    char *hexByteToCharByte(unsigned char src);
    void strToHex(char *dest, const char *src, unsigned int n);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _TRANSFORM_ASCII_H