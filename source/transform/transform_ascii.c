#include "transform_ascii.h"
#include <stdio.h>

char charToHex(const char *src)
{
    if (*src >= '0' && *src <= '9')
    {
        return (*src - '0');
    }
    else if (*src >= 'A' && *src <= 'F')
    {
        return (*src - 'A' + 10);
    }
    else if (*src >= 'a' && *src <= 'f')
    {
        return (*src - 'a' + 10);
    }
    else
    {
        return '\0';
    }
}

unsigned char charByteToHexByte(char *src)
{
    unsigned char hexByte = 0;
    unsigned char tmpSrc[2] = {'\0'};

    for (int i = 0; i < 2; i++)
    {
        if (src[i] >= '0' && src[i] <= '9')
        {
            tmpSrc[i] = src[i] - '0';
        }
        else if (src[i] >= 'A' && src[i] <= 'F')
        {
            tmpSrc[i] = src[i] - 'A' + 0x0A;
        }
        else if (src[i] >= 'a' && src[i] <= 'f')
        {
            tmpSrc[i] = src[i] - 'a' + 0x0A;
        }
    }

    hexByte = (tmpSrc[0] << 4) + tmpSrc[1];

    return hexByte;
}

char *hexByteToCharByte(unsigned char src)
{
    static unsigned char charBuffer[3] = {'\0'};
    unsigned char tmpSrc[2] = {'\0'};

    tmpSrc[0] = (src & 0xF0) >> 4;
    tmpSrc[1] = src & 0x0F;

    for (int i = 0; i < 2; i++)
    {
        if (tmpSrc[i] >= 0x00 && tmpSrc[i] <= 0x09)
        {
            charBuffer[i] = tmpSrc[i] + '0';
        }
        else if (tmpSrc[i] >= 0x0A && tmpSrc[i] <= 0x0F)
        {
            charBuffer[i] = tmpSrc[i] - 0x0A + 'A';
        }
    }

    return charBuffer;
}

void strToHex(char *dest, const char *src, unsigned int n)
{
    char hex = 0;
    if (n)
    {
        for (unsigned int i = 0; i < n; i++)
        {
            hex = charToHex(&src[i * 2]) << 4;
            hex += charToHex(&src[(i * 2) + 1]);
            *(dest + i) = hex;
        }
    }
}