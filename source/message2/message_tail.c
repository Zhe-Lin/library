#include "message_component.h"

typedef struct MESSAGE_COMPONENT_TAIL
{
  baseComponent;

  uint8_t *specifyTail;
}messageComponentTail;

bool message_tailCheck(void *specifyModule, void *specifyComponent);
void message_tailPostProcess(void *specifyModule, void *specifyComponent, bool result);
void message_tailFree(void *specifyComponent);

void message_bindingTail(void *module, const char *registerName, const char *tail, int tailBytesNum) {
  messageComponentTail *component = (messageComponentTail *)message_assignStaticSpace((messageModule *)module, registerName, sizeof(messageComponentTail), tailBytesNum);

  component->specifyTail = calloc(strlen(tail) + 1, sizeof(uint8_t));
  memcpy(component->specifyTail, tail, strlen(tail));
  memcpy(component->sendData, tail, strlen(tail));
  component->receivePromptlyProcess = message_tailCheck;
  component->receivePromptlyPostProcess = message_tailPostProcess;
  component->free = message_tailFree;
}

bool message_tailCheck(void *specifyModule, void *specifyComponent) {
  messageComponentTail *component = (messageComponentTail *)specifyComponent;

  if (memcmp(component->receiveData, component->specifyTail, component->receiveBytesNum) == 0)
    return true;

  return false;
}

void message_tailPostProcess(void *specifyModule, void *specifyComponent, bool result) {
  message_sendSaveResponse(specifyModule, result);
}

void message_tailFree(void *specifyComponent) {
  messageComponentTail *component = (messageComponentTail *)specifyComponent;

  free(component->specifyTail);
}