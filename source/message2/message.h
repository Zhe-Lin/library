#ifndef MESSAGE_H
#define MESSAGE_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdbool.h>

  void *message_moduleNew(const char *moduleName);
  void message_moduleFree(void *specifyModule);

  void message_sendSetCallBack(void *specifyModule, void *port, int (*sendCallBack)(void *port, const char *data, int dataLength));
  void message_sendSaveData(void *specifyModule, const char *componentName, const char *data, int dataLength);
  void message_sendSetResponse(void *specifyModule, const char *componentName, const char *resultTrue, const char *resultFalse);

  void message_receiveHandler(void *specifyModule, const char *data, int dataLen);
  void message_idleHandler(void *specifyModule);

  void message_registerProcess(void *specifyModule, const char *registerName, const char *componentName, bool (*process)(void *specifyModule, void *specifyComponent));

  void message_bindingHead(void *module, const char *registerName, const char *head, int headBytesNum);
  void message_bindingStaticCommand(void *module, const char *registerName, int dataBytesNum);
  void message_bindingVariableCommand(void *module, const char *registerName, int lengthBytesNum);
  void message_bindingStaticData(void *module, const char *registerName, int dataBytesNum);
  void message_bindingVariableData(void *module, const char *registerName, int lengthBytesNum);
  void message_bindingCRC8(void *module, const char *registerName, const char *startCRC8);
  void message_bindingTail(void *module, const char *registerName, const char *tail, int tailBytesNum);

  int message_componentGetReceiveDataLength(void *component);
  const char *message_componentGetReceiveData(void *component);
  int message_componentGetSendDataLength(void *component);
  const char *message_componentGetSendData(void *component);
  unsigned char *message_componentGetNumberConvertBytesBigEndian(int dataLength, int bytesNum);
  unsigned int message_componentGetBytesConvertNumberBigEndian(const char *bytes, int bytesNum);

#ifdef __cplusplus
}
#endif

#endif // MESSAGE_H

