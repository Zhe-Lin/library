#include "message.h"
#include "message_component.h"

void message_handler(char *data, int dataLen) {
  printf("message_handler test\r\n");
  // messageModule *otaMessage1 = NULL;
  // otaMessage1 = message_moduleNew("ota");

  // message_assignStaticSpace(otaMessage1, "static1", sizeof(messageComponent), 2);
  // message_assignStaticSpace(otaMessage1, "static2", sizeof(messageComponent), 2);
  // message_printf(otaMessage1);
  // message_moduleFree(otaMessage1);
}

void message_registerProcess(void *specifyModule, const char *registerName, const char *componentName, bool (*process)(void *specifyModule, void *specifyComponent)) {
  message_assignProcess((messageModule *)specifyModule, registerName, componentName, process);
}

void message_receiveHandler(void *specifyModule, const char *data, int dataLen) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *current = module->components.specify;
  messageState currentState = stateUndo;
  uint32_t processedDataLen = 0;
  bool result = false;

  // printf("========== start ==========\r\n");
  do {
    // printf("processedDataLen:%d\r\n", processedDataLen);
    // printf("current:%p\r\n", current);
    currentState = message_componentGetState(current);
    if (currentState == stateUndo) {
      message_componentReset(current, current->receiveData, current->receiveBytesNum);
      message_componentSetState(current, stateDoing);
    }
    else if (currentState == stateDone) {
      // printf("stateDone, next component:%p\r\n", current);
      current = message_componentGetNext(current);
      continue;
    }

    // printf("componentName:%s\r\n", current->spaceName);
    // for (uint32_t i = 0; i < dataLen; i++)
    // {
    //   printf("%02X ", (unsigned char)data[i]);
    // }
    // printf("\r\n");

    // printf("processedDataLen:%d\r\n", processedDataLen);
    processedDataLen += message_componentReceiveSaveData(module, current, (uint8_t *)(data + processedDataLen), dataLen - processedDataLen);

    // printf("saved data\r\n");
    if (message_componentIsDataFull(module, current) == false)
      continue;

    // printf("componentName:%s\r\n", current->spaceName);
    // unsigned int dataLength = message_componentGetReceiveDataLength(current);
    // printf("receive data(%d): \r\n", dataLength);
    // for (uint32_t i = 0; i < dataLength; i++)
    // {
    //   printf("%02X ", (unsigned char)message_componentGetReceiveData(current)[i]);
    // }
    // printf("\r\n");

    message_componentReceivePromptlyPreProcess(module, current);
    result = message_componentReceivePromptlyProcess(module, current);
    message_componentReceivePromptlyPostProcess(module, current, result);

    message_componentForwardOrReturn(module, &current, result);
  } while ((dataLen > processedDataLen) && (current != NULL));
  // printf("========== finish ==========\r\n");
}

void message_receiveIdleHandler(void *specifyModule) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *current = message_componentGetLast(module);

  if (message_componentGetState(current) != stateDone)
    return;
  else
    current = message_componentGetFirst(module);

  // printf("========== start ==========\r\n");
  do {
    // printf("current:%p\r\n", current);
    // printf("handler:%s\r\n", current->spaceName);
    message_componentReceiveIdleProcess(module, current);
    message_componentReceiveIdlePostProcess(module, current);

    current = message_componentGetNext(current);
  } while (current != NULL);
  // printf("========== finish ==========\r\n");

  message_componentForwardOrReturn(module, &current, 0);
}

void message_sendIdleHandler(void *specifyModule) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *current = message_componentGetFirst(module);
  bool result = false;

  if (message_sendGetState(module) == false)
    return;

  // printf("========== start ==========\r\n");
  do {
    // printf("current:%p\r\n", current);
    // printf("handler:%s\r\n", current->spaceName);

    // printf("componentName:%s\r\n", current->spaceName);
    // for (uint32_t i = 0; i < current->sendBytesNum; i++)
    // {
    //   printf("%x ", current->sendData[i]);
    // }
    // printf("\r\n");
    message_componentSendIdlePreProcess(module, current);
    message_componentSendDataProcess(module, current);
    message_componentSendIdlePostProcess(module, current);

    current = message_componentGetNext(current);
  } while (current != NULL);
  // printf("========== finish ==========\r\n");

  message_sendSetState(module, false);
}

void message_idleHandler(void *specifyModule) {
  message_sendIdleHandler(specifyModule);
  message_receiveIdleHandler(specifyModule);
}
