#include "message_component.h"

typedef struct MESSAGE_COMPONENT_DATA_LENGTH
{
  baseComponent;
}messageComponentDataLength;

typedef struct MESSAGE_COMPONENT_DATA
{
  baseComponent;
}messageComponentData;

void message_dataLengthPreProcess(void *specifyModule, void *specifyComponent);

void message_bindingStaticData(void *module, const char *registerName, int dataBytesNum) {
  messageComponentData *component = (messageComponentData *)message_assignStaticSpace((messageModule *)module, registerName, sizeof(messageComponentData), dataBytesNum);
}

void message_bindingVariableData(void *module, const char *registerName, int lengthBytesNum) {
  char dataLengthName[strlen(registerName) + 4];
  memset(dataLengthName, '\0', sizeof(dataLengthName));
  snprintf(dataLengthName, sizeof(dataLengthName), "%sLen", registerName);

  messageComponentDataLength *componentDataLength = (messageComponentDataLength *)message_assignStaticSpace((messageModule *)module, dataLengthName, sizeof(messageComponentDataLength), lengthBytesNum);
  componentDataLength->receivePromptlyPreProcess = message_dataLengthPreProcess;

  messageComponentData *componentData = (messageComponentData *)message_assignVariableSpace((messageModule *)module, registerName, sizeof(messageComponentData));
}

unsigned int message_dataLengthCalculateCalculateBigEndian(messageModule *module, messageComponent *component) {
  unsigned int dataLength = message_componentGetBytesConvertNumberBigEndian(message_componentGetReceiveData(component), message_componentGetReceiveDataLength(component));

  return dataLength;
}

void message_dataLengthUpdate(messageModule *module, messageComponent *component, unsigned int dataLength) {
  messageComponentDataLength *componentDataLength = (messageComponentDataLength *)component;
  messageComponentData *componentData = (messageComponentData *)componentDataLength->next;

  componentData->receiveBytesNum = dataLength;
  free(componentData->receiveData);
  componentData->receiveData = calloc(componentData->receiveBytesNum + 1, sizeof(uint8_t));
}

void message_dataLengthPreProcess(void *specifyModule, void *specifyComponent) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *component = (messageComponent *)specifyComponent;
  unsigned int dataLength = 0;

  dataLength = message_dataLengthCalculateCalculateBigEndian(module, component);
  message_dataLengthUpdate(module, component, dataLength);
}
