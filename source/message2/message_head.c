#include "message_component.h"

typedef struct MESSAGE_COMPONENT_HEAD
{
  baseComponent;

  uint8_t *specifyHead;
}messageComponentHead;

bool message_headCheck(void *specifyModule, void *specifyComponent);
void message_headFree(void *specifyComponent);

void message_bindingHead(void *module, const char *registerName, const char *head, int headBytesNum) {
  messageComponentHead *component = (messageComponentHead *)message_assignStaticSpace((messageModule *)module, registerName, sizeof(messageComponentHead), headBytesNum);

  component->specifyHead = calloc(strlen(head) + 1, sizeof(uint8_t));
  memcpy(component->specifyHead, head, strlen(head));
  memcpy(component->sendData, head, strlen(head));
  component->receivePromptlyProcess = message_headCheck;
  component->free = message_headFree;
}

bool message_headCheck(void *specifyModule, void *specifyComponent) {
  messageComponentHead *component = (messageComponentHead *)specifyComponent;

  if (memcmp(component->receiveData, component->specifyHead, component->receiveBytesNum) == 0)
    return true;

  return false;
}

void message_headFree(void *specifyComponent) {
  messageComponentHead *component = (messageComponentHead *)specifyComponent;

  free(component->specifyHead);
}