#ifndef MESSAGE_COMPONENT_H
#define MESSAGE_COMPONENT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define baseComponent \
  struct {\
    /* method */\
    void (*receivePromptlyPreProcess)(void *specifyModule, void *specifyComponent); \
    bool (*receivePromptlyProcess)(void *specifyModule, void *specifyComponent);\
    void (*receivePromptlyPostProcess)(void *specifyModule, void *specifyComponent, bool result);\
    void (*receiveIdlePreProcess)(void *specifyModule, void *specifyComponent);\
    bool (*receiveIdleProcess)(void *specifyModule, void *specifyComponent);\
    void (*receiveIdlePostProcess)(void *specifyModule, void *specifyComponent);\
    void (*sendIdlePreProcess)(void *specifyModule, void *specifyComponent);\
    void (*sendIdlePostProcess)(void *specifyModule, void *specifyComponent);\
    void (*free)(void *specifyComponent);\
    /* property */\
    messageComponent *next;\
    char *spaceName;\
    messageType type;\
    messageState state;\
    unsigned int receiveBytesNum;\
    unsigned char *receiveData;\
    unsigned int sendBytesNum;\
    unsigned char *sendData;\
  }

  typedef enum MESSAGE_TYPE
  {
    typeStaticSpace = 0,
    typeVariableSpace
  } messageType;

  typedef enum MESSAGE_STATE
  {
    stateUndo = 0,
    stateDoing,
    stateDone,
  } messageState;

  typedef struct MESSAGE_COMPONENT messageComponent;
  typedef struct MESSAGE_PROCESS messageProcess;

  typedef struct MESSAGE_COMPONENT
  {
    baseComponent;
  }messageComponent;

  typedef struct MESSAGE_COMPONENT_GROUP
  {
    messageComponent *specify;
    uint32_t number;
    uint32_t index;
  }messageComponentGroup;

  typedef struct MESSAGE_PROCESS
  {
    bool (**map)(void *specifyModule, void *specifyComponent);
    bool (*save)(void *specifyModule, void *specifyComponent);

    messageProcess *next;
    char *name;
  }messageProcess;

  typedef struct MESSAGE_PROCESS_GROUP
  {
    messageProcess *specify;
    uint32_t number;
  }messageProcessGroup;

  typedef struct MESSAGE_RESPONSE
  {
    messageComponent *component;
    char *resultTrue;
    char *resultFalse;
  }messageResponse;

  typedef struct MESSAGE_SEND
  {
    int (*callBack)(void *port, const char *data, int dataLength);

    bool state;
    void *port;
  }messageSend;

  typedef struct MESSAGE_MODULE
  {
    uint8_t *moduleName;
    messageComponentGroup components;
    messageProcessGroup processes;
    messageSend send;
    messageResponse response;
  } messageModule;

  void *message_moduleNew(const char *moduleName);
  void message_moduleFree(void *specifyModule);

  void *message_processGetSpecify(void *specifyModule, const char *mapName);
  bool message_processSpecify(void *specifyModule, void *specifyComponent);

  void message_sendSetCallBack(void *specifyModule, void *port, int (*sendCallBack)(void *port, const char *data, int dataLength));
  void message_sendSetResponse(void *specifyModule, const char *componentName, const char *resultTrue, const char *resultFalse);
  bool message_sendGetState(void *specifyModule);
  void message_sendSetState(void *specifyModule, bool state);
  void message_sendSaveData(void *specifyModule, const char *componentName, const char *data, int dataLength);
  void message_sendSaveResponse(void *specifyModule, bool result);

  void message_responseSaveSendData(void *specifyModule, void *specifyComponent);

  messageState message_componentGetState(messageComponent *component);
  void message_componentSetState(messageComponent *component, messageState state);
  void message_componentClearAllStatus(void *specifyModule);
  messageComponent *message_componentGetNext(messageComponent *component);
  messageComponent *message_componentGetFirst(messageModule *module);
  messageComponent *message_componentGetLast(messageModule *module);
  messageComponent *message_componentGetSpecify(void *specifyModule, const char *componentName);
  void *message_componentGetSpecifyFront(void *specifyModule, const char *componentName);
  unsigned int message_componentReceiveSaveData(messageModule *module, messageComponent *component, uint8_t *data, uint32_t bytesNum);
  bool message_componentIsDataFull(messageModule *module, messageComponent *component);
  void message_componentReceivePromptlyPreProcess(messageModule *module, messageComponent *component);
  bool message_componentReceivePromptlyProcess(messageModule *module, messageComponent *component);
  void message_componentReceivePromptlyPostProcess(messageModule *module, messageComponent *component, bool result);
  bool message_componentReceiveIdleProcess(messageModule *module, messageComponent *component);
  void message_componentReceiveIdlePostProcess(messageModule *module, messageComponent *component);
  void message_componentSendIdlePreProcess(messageModule *module, messageComponent *component);
  void message_componentSendIdlePostProcess(messageModule *module, messageComponent *component);
  void message_componentSendDataProcess(messageModule *module, messageComponent *component);
  void message_componentForwardOrReturn(messageModule *module, messageComponent **component, bool result);
  void message_componentReset(void *specifyComponent, unsigned char *specifyResetData, unsigned int specifyResetDataLength);
  int message_componentGetReceiveDataLength(void *component);
  const char *message_componentGetReceiveData(void *component);
  int message_componentGetSendDataLength(void *component);
  const char *message_componentGetSendData(void *component);
  unsigned int message_componentGetDataBufferLength(void *specifyComponent, void *startComponent, int (*dataLengthType)(void *component));
  unsigned char *message_componentGetDataBuffer(void *specifyComponent, void *startComponent, unsigned int bufferLength, const char *(*dataBufferType)(void *component), int (*dataLengthType)(void *component));
  unsigned char *message_componentGetNumberConvertBytesBigEndian(int number, int maxBytesNum);
  unsigned int message_componentGetBytesConvertNumberBigEndian(const char *bytes, int bytesNum);

  messageComponent *message_assignStaticSpace(messageModule *module, const char *spaceName, unsigned char spaceSize, unsigned int bytesNum);
  messageComponent *message_assignVariableSpace(messageModule *module, const char *spaceName, unsigned char spaceSize);
  messageProcess *message_assignProcess(messageModule *module, const char *mapName, const char *componentName, bool (*process)(void *specifyModule, void *specifyComponent));

  void message_printf(messageModule *module);

#ifdef __cplusplus
}
#endif

#endif // MESSAGE_COMPONENT_H

