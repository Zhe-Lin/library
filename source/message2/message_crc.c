#include "message_component.h"
#include "encrypt/encrypt.h"

typedef struct MESSAGE_COMPONENT_CRC
{
  baseComponent;

  char *specifyStartCRC;
}messageComponentCRC;

bool message_CRC8Check(void *specifyModule, void *specifyComponent);
void message_CRC8Make(void *specifyModule, void *specifyComponent);
void message_CRC8PostProcess(void *specifyModule, void *specifyComponent, bool result);
void message_CRC8Free(void *specifyComponent);

void message_bindingCRC8(void *module, const char *registerName, const char *startCRC8) {
  messageComponentCRC *component = (messageComponentCRC *)message_assignStaticSpace((messageModule *)module, registerName, sizeof(messageComponentCRC), 1);

  component->specifyStartCRC = calloc(strlen(startCRC8) + 1, sizeof(uint8_t));
  memcpy(component->specifyStartCRC, startCRC8, strlen(startCRC8));
  component->receivePromptlyProcess = message_CRC8Check;
  component->receivePromptlyPostProcess = message_CRC8PostProcess;
  component->sendIdlePreProcess = message_CRC8Make;
  component->free = message_CRC8Free;
}

bool message_CRC8Check(void *specifyModule, void *specifyComponent) {
  messageComponentCRC *componentCRC = (messageComponentCRC *)specifyComponent;
  messageComponent *startComponent = message_componentGetSpecifyFront(specifyModule, componentCRC->specifyStartCRC);
  uint32_t calculateBufferLength;
  uint8_t *calculateBuffer;
  uint8_t crc[1] = { '\0' };

  calculateBufferLength = message_componentGetDataBufferLength(componentCRC, startComponent, message_componentGetReceiveDataLength);
  calculateBuffer = message_componentGetDataBuffer(componentCRC, startComponent, calculateBufferLength, message_componentGetReceiveData, message_componentGetReceiveDataLength);

  // printf("calculateBufferLength:%d\r\n", calculateBufferLength);
  // printf("buffer:");
  // for (size_t i = 0; i < calculateBufferLength; i++)
  // {
  //   printf("0x%02X ", *(calculateBuffer + i));
  // }
  // printf("\r\n");

  crc[0] = crc8(calculateBuffer, calculateBufferLength);
  // printf("calculate crc:0x%02X\r\n", crc[0]);
  // printf("receive crc:0x%02X\r\n", componentCRC->receiveData[0]);

  free(calculateBuffer);
  if (memcmp(componentCRC->receiveData, crc, componentCRC->receiveBytesNum) == 0)
    return true;

  return false;
}

void message_CRC8Make(void *specifyModule, void *specifyComponent) {
  messageComponentCRC *componentCRC = (messageComponentCRC *)specifyComponent;
  messageComponent *startComponent = message_componentGetSpecifyFront(specifyModule, componentCRC->specifyStartCRC);
  uint32_t calculateBufferLength;
  uint8_t *calculateBuffer;

  calculateBufferLength = message_componentGetDataBufferLength(componentCRC, startComponent, message_componentGetSendDataLength);
  calculateBuffer = message_componentGetDataBuffer(componentCRC, startComponent, calculateBufferLength, message_componentGetSendData, message_componentGetSendDataLength);

  componentCRC->sendData[0] = crc8(calculateBuffer, calculateBufferLength);

  free(calculateBuffer);
}

void message_CRC8PostProcess(void *specifyModule, void *specifyComponent, bool result) {
  message_sendSaveResponse(specifyModule, result);
}

void message_CRC8Free(void *specifyComponent) {
  messageComponentCRC *componentCRC = (messageComponentCRC *)specifyComponent;

  free(componentCRC->specifyStartCRC);
}
