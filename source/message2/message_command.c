#include "message_component.h"

typedef struct MESSAGE_COMPONENT_COMMAND_LENGTH
{
  baseComponent;
}messageComponentCommandLength;

typedef struct MESSAGE_COMPONENT_COMMAND
{
  baseComponent;
}messageComponentCommand;

void message_commandLengthPreProcess(void *specifyModule, void *specifyComponent);
void message_commandPreProcess(void *specifyModule, void *specifyComponent);
bool message_commandProcessSpecify(void *specifyModule, void *specifyComponent);

void message_bindingStaticCommand(void *module, const char *registerName, int dataBytesNum) {
  messageComponentCommand *component = (messageComponentCommand *)message_assignStaticSpace((messageModule *)module, registerName, sizeof(messageComponentCommand), dataBytesNum);
  component->receivePromptlyPreProcess = message_commandPreProcess;
  component->receiveIdleProcess = message_commandProcessSpecify;
}

void message_bindingVariableCommand(void *module, const char *registerName, int lengthBytesNum) {
  char commandLengthName[strlen(registerName) + 4];
  memset(commandLengthName, '\0', sizeof(commandLengthName));
  snprintf(commandLengthName, sizeof(commandLengthName), "%sLen", registerName);

  messageComponentCommandLength *componentCommandLength = (messageComponentCommandLength *)message_assignStaticSpace((messageModule *)module, commandLengthName, sizeof(messageComponentCommandLength), lengthBytesNum);
  componentCommandLength->receivePromptlyPreProcess = message_commandLengthPreProcess;

  messageComponentCommand *componentCommand = (messageComponentCommand *)message_assignVariableSpace((messageModule *)module, registerName, sizeof(messageComponentCommand));
  componentCommand->receivePromptlyPreProcess = message_commandPreProcess;
  componentCommand->receiveIdleProcess = message_commandProcessSpecify;
}

unsigned int message_commandLengthCalculateBigEndian(messageModule *module, messageComponent *component) {
  unsigned int commandLength = message_componentGetBytesConvertNumberBigEndian(message_componentGetReceiveData(component), message_componentGetReceiveDataLength(component));

  return commandLength;
}

void message_commandLengthUpdate(messageModule *module, messageComponent *component, unsigned int commandLength) {
  messageComponentCommandLength *componentCommandLength = (messageComponentCommandLength *)component;
  messageComponentCommand *componentCommand = (messageComponentCommand *)componentCommandLength->next;

  componentCommand->receiveBytesNum = commandLength;
  free(componentCommand->receiveData);
  componentCommand->receiveData = calloc(componentCommand->receiveBytesNum + 1, sizeof(uint8_t));
}

void message_commandLengthPreProcess(void *specifyModule, void *specifyComponent) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *component = (messageComponent *)specifyComponent;
  unsigned int commandLength = 0;

  commandLength = message_commandLengthCalculateBigEndian(module, component);
  message_commandLengthUpdate(module, component, commandLength);
  message_responseSaveSendData(module, component);
}

void message_commandPreProcess(void *specifyModule, void *specifyComponent) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *component = (messageComponent *)specifyComponent;
  message_responseSaveSendData(module, component);
}

bool message_commandProcessSpecify(void *specifyModule, void *specifyComponent) {
  return message_processSpecify(specifyModule, specifyComponent);
}
