#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "time_process.h"
#include "common/custom_utility.h"

int32_t checkLeapYear(int32_t iYear)
{
    if ((iYear % 4 == 0 && iYear % 100 != 0) || (iYear % 400 == 0))
        return 1;

    return 0;
}

void unixToTime(uint64_t InUnixTime, int32_t *iYear, int32_t *iMonth, int32_t *iDate, int32_t *iHour, int32_t *iMin, int32_t *iSec)
{
    int64_t iDaysofMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int64_t iDaysofMonth2[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int64_t iTempYear, iTempMonth;
    int64_t TotalDay;
    int64_t lTempDay;
    int64_t SurplusSec;
    int64_t lLeapDay;
    int64_t lTemp;

    TotalDay = InUnixTime / (24 * 60 * 60);
    SurplusSec = InUnixTime % (24 * 60 * 60);

    iTempYear = 0;
    lTempDay = 0;
    while (1) // Get Years
    {
        if (checkLeapYear(iTempYear + 1970))
            lLeapDay = 366;
        else
            lLeapDay = 365;

        lTempDay += lLeapDay;

        if ((TotalDay - lTempDay) < 0)
        {
            lTempDay -= lLeapDay;
            break;
        }
        iTempYear++;
    }

    lTempDay = TotalDay - lTempDay; //Surplus days

    lTemp = 0;
    for (iTempMonth = 0; iTempMonth < 12; iTempMonth++)
    {
        if (checkLeapYear(iTempYear + 1970))
            lLeapDay = iDaysofMonth2[iTempMonth];
        else
            lLeapDay = iDaysofMonth[iTempMonth];

        lTemp += lLeapDay;
        if (lTemp > lTempDay)
        {
            lTemp -= lLeapDay;
            break;
        }
    }

    *iYear = iTempYear + 1970;
    *iMonth = iTempMonth + 1;
    *iDate = lTempDay - lTemp + 1;
    *iHour = SurplusSec / (60 * 60);
    SurplusSec = SurplusSec % (60 * 60);
    *iMin = SurplusSec / 60;
    *iSec = SurplusSec % 60;

    //return 0;
}

uint32_t timeToUnix(int32_t iYear, int32_t iMonth, int32_t iDate, int iHour, int iMin, int iSec)
{
    int32_t iDaysofMonth[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
    int32_t iTemp;
    int32_t LeapYearCount;
    uint32_t lSec;

    iTemp = iYear - 1970;
    LeapYearCount = 0;

    LeapYearCount = ((iTemp + 2) / 4); //計算1970 ~ iYear 總共經過幾個閏年

    if ((iTemp + 2) % 4 == 0) //如果iYear 是閏年
    {
        if (iMonth <= 2) //iYear 月份尚未超過02/29,要減1天回來
        {
            LeapYearCount--;
        }
        else if ((iMonth == 2) && (iDate == 29)) //iYear 月份剛好02/29,要加1天
        {
            LeapYearCount++;
        }
    }

    lSec = LeapYearCount + iTemp * 365 + iDaysofMonth[iMonth - 1] + iDate - 1;
    lSec = lSec * 24 + iHour;
    lSec = lSec * 60 + iMin;
    lSec = lSec * 60 + iSec;

    return lSec;
}

int32_t dateCompare(Date dt1, Date dt2)
{
    int32_t date1[6];
    int32_t date2[6];
    int32_t i;
    int32_t ret = 0;
    date1[0] = dt1.Year;
    date1[1] = dt1.Month;
    date1[2] = dt1.Day;
    date1[3] = dt1.Hour;
    date1[4] = dt1.Minute;
    date1[5] = dt1.Second;

    date2[0] = dt2.Year;
    date2[1] = dt2.Month;
    date2[2] = dt2.Day;
    date2[3] = dt2.Hour;
    date2[4] = dt2.Minute;
    date2[5] = dt2.Second;
    for (i = 0; i < 6; i++)
    {
        if (date1[i] > date2[i])
        {
            ret = 1;
            break;
        }
        else if (date1[i] < date2[i])
        {
            ret = -1;
            break;
        }
    }
    return ret;
}

void dosDateToDate(uint8_t *dosDate, Date *dt)
{
    uint16_t sDosDate;
    sDosDate = UnsignedValue2ByteToInt(dosDate);
    dt->Year = ((sDosDate >> 9) & 0x7F) + 1980;
    dt->Month = (sDosDate >> 5) & 0x0F;
    dt->Day = sDosDate & 0x1F;
    dt->Hour = 23;
    dt->Minute = 59;
    dt->Second = 59;
}

uint16_t offsetDate(IN OUT char *date, IN int offset)
{
    char dateBuffer[8] = {'\0'};
    int year = 0, month = 0, day = 0;
    struct tm dateStruct = {'\0'};

    /* initialize */
    memset(dateBuffer, '\0', sizeof(dateBuffer));
    memcpy(dateBuffer, date, 4);
    year = atoi(dateBuffer);

    memset(dateBuffer, '\0', sizeof(dateBuffer));
    memcpy(dateBuffer, date + 4, 2);
    month = atoi(dateBuffer);

    memset(dateBuffer, '\0', sizeof(dateBuffer));
    memcpy(dateBuffer, date + 6, 2);
    day = atoi(dateBuffer);

    dateStruct.tm_year = year - 1900; /* The number of years since 1900   */
    dateStruct.tm_mon = month - 1;    /* month, range 0 to 11 */
    dateStruct.tm_mday = day;

    /* modify */
    dateStruct.tm_mday += offset;
    mktime(&dateStruct);

    sprintf(date, "%04d%02d%02d", dateStruct.tm_year + 1900, dateStruct.tm_mon + 1, dateStruct.tm_mday);

    return EXECUTE_OK;
}
