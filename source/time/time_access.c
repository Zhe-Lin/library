#include <time.h>
#include <string.h>
#include <stdio.h>

#include "time_access.h"

void getCurrentDate(IN OUT char *date, IN uint32_t size)
{
    time_t rawTime;
    struct tm *timeInfo;
    memset(date, '\0', size);

    time(&rawTime);
    timeInfo = localtime(&rawTime);
    sprintf(date, "%04d%02d%02d", timeInfo->tm_year + 1900, timeInfo->tm_mon + 1, timeInfo->tm_mday);
}
