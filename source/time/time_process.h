#ifndef _TIME_PROCESS_H
#define _TIME_PROCESS_H

#include <stdint.h>

#include "common/custom_errors.h"
#include "common/custom_types.h"

typedef struct
{
    int32_t Year;   // 2012,2011....
    int32_t Month;  // 1~12
    int32_t Day;    // 1~31
    int32_t Hour;   // 0~23
    int32_t Minute; //0~59
    int32_t Second; //0~59
} Date;

//--- Data Definition---

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

extern int32_t checkLeapYear(int32_t iYear);
extern void unixToTime(uint64_t InUnixTime, int32_t *iYear, int32_t *iMonth, int32_t *iDate, int32_t *iHour, int32_t *iMin, int32_t *iSec);
extern uint32_t timeToUnix(int32_t iYear, int32_t iMonth, int32_t iDate, int32_t iHour, int32_t iMin, int32_t iSec);
extern int32_t dateCompare(Date dt1, Date dt2);
extern void dosDateToDate(uint8_t *dosDate, Date *dt);
uint16_t offsetDate(IN OUT char *date, IN int offset);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _TIME_PROCESS_H
