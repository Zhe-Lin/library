#ifndef _CUSTOM_UTILITY_H
#define _CUSTOM_UTILITY_H

#include <stdint.h>

//--- Data Definition---

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

extern int32_t Value4ByteComp_le(uint8_t *, uint8_t *);
extern int32_t Value4ByteToInt(uint8_t *);
extern uint16_t UnsignedValue2ByteToIntLe(uint8_t *);
extern uint16_t UnsignedValue2ByteToInt(uint8_t *byteValue);
extern void ValueIntTo2Byte(uint8_t *, int32_t);
extern void UnsignedValueIntTo2Byte(uint8_t *, uint16_t);
extern void ValueIntTo4ByteLe(uint8_t *byteAry, int32_t value);
extern uint32_t UnsignedValue4ByteToUIntLe(uint8_t *byteValue);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _CUSTOM_UTILITY_H
