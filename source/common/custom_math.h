#ifndef _CUSTOM_MATH_H
#define _CUSTOM_MATH_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int numberDouble(int number);
int numberQuadruple(int number);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _CUSTOM_MATH_H