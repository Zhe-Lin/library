#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "custom_utility.h"

int32_t Value4ByteComp_le(uint8_t *ut1, uint8_t *ut2)
{
    int32_t ret = 0;
    int32_t i;
    for (i = 3; i >= 0; i--)
    {
        if (ut1[i] > ut2[i])
        {
            ret = 1;
            break;
        }
        else if (ut1[i] < ut2[i])
        {
            ret = -1;
            break;
        }
    }
    return ret;
}

int32_t Value4ByteToInt(uint8_t *byteValue)
{
    int32_t i;
    int32_t intValue = 0;
    for (i = 3; i >= 0; i--)
        intValue = (intValue << 8) + byteValue[i];
    return intValue;
}

uint16_t UnsignedValue2ByteToIntLe(uint8_t *byteValue)
{
    int32_t i;
    uint16_t intValue = 0;
    for (i = 1; i >= 0; i--)
        intValue = (intValue << 8) + byteValue[i];
    return intValue;
}

uint16_t UnsignedValue2ByteToInt(uint8_t *byteValue)
{
    int32_t i;
    uint16_t intValue = 0;
    for (i = 0; i <= 1; i++)
        intValue = (intValue << 8) + byteValue[i];
    return intValue;
}

void ValueIntTo2Byte(uint8_t *byteAry, int32_t value)
{
    int32_t i;
    for (i = 0; i < 2; i++)
    {
        byteAry[i] = value & 0xFF;
        value = value >> 8;
    }
}

void UnsignedValueIntTo2Byte(uint8_t *byteAry, uint16_t value)
{
    int32_t i;
    for (i = 0; i < 2; i++)
    {
        byteAry[i] = value & 0xFF;
        value = value >> 8;
    }
}

void ValueIntTo4ByteLe(uint8_t *byteAry, int32_t value)
{
    int32_t i;
    for (i = 0; i < 4; i++)
    {
        byteAry[i] = value & 0xFF;
        value = value >> 8;
    }
}

uint32_t UnsignedValue4ByteToUIntLe(uint8_t *byteValue)
{
    int32_t i;
    uint32_t uintValue = 0;
    for (i = 3; i >= 0; i--)
        uintValue = (uintValue << 8) + byteValue[i];
    return uintValue;
}
