#include "custom_log.h"
#include "file/file_process.h"
#include "file/file_settings.h"
#include "file/file_access.h"

fileObject customLog = {'\0'};

void enableLog(void)
{
    createLog(&customLog);
}

void setLogName(IN const char *name)
{
    fileRename(&customLog, name);
}

void setLogPath(IN const char *path)
{
    folderRename(&customLog, path);
}

void setAllLogMaxSpace(IN uint32_t maxSpace)
{
    setAllFileMaxSpace(&customLog, maxSpace);
}

void setOneLogMaxSpace(IN uint32_t maxSpace)
{
    setOneFileMaxSpace(&customLog, maxSpace);
}

void setLogExistDay(IN uint32_t existDay)
{
    setFileExistDay(&customLog, existDay);
}

void setLogSaveMethod(IN uint32_t type)
{
    initialSaveMethod(&customLog, (saveFileMethod)type);
}
