#include "custom_math.h"
#include <time.h>
#include <string.h>
#include <stdio.h>

int numberDouble(int number)
{
    return number * 2;
}

int numberQuadruple(int number)
{
    return numberDouble(number) * 2;
}
