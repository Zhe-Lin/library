#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include "common/custom_errors.h"
#include "file_access.h"

uint16_t createFile(IN const char *path)
{
    FILE *pFile;

    pFile = fopen(path, FILE_MODE_W);

    if (NULL == pFile)
    {
        return ERR_MISC_FILE_OPEN_ERROR;
    }

    fclose(pFile);

    return EXECUTE_OK;
}

uint16_t deleteFile(IN const char *path)
{
    int result = unlink(path);
    return EXECUTE_OK;
}

int32_t deleteSpecifyFile(IN const char *folderName, IN const char *deleteFileName)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    int32_t result = -1;

    if (pDir)
    {
        struct dirent *pDirStatus;

        result = 0;
        while ((result == 0) && (pDirStatus = readdir(pDir)))
        {
            int32_t subResult = -1;
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        subResult = deleteSpecifyFile(buf, deleteFileName);
                    else
                    {
                        if (strstr(pDirStatus->d_name, deleteFileName) != NULL)
                            subResult = deleteFile(buf);
                        else
                            subResult = 0;
                    }
                }
                free(buf);
            }
            result = subResult;
        }
        closedir(pDir);
    }

    return result;
}

int32_t deleteSpecifyDayAgo(IN const char *folderName, IN const char *fileName, IN const char *deleteDate)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    int fileNameLength = strlen(fileName);
    int dateLength = strlen(deleteDate);
    char dateOfFileName[dateLength + 1];
    int32_t result = -1;

    memset(dateOfFileName, '\0', dateLength + 1);

    if (pDir)
    {
        struct dirent *pDirStatus;

        result = 0;
        while ((result == 0) && (pDirStatus = readdir(pDir)))
        {
            int32_t subResult = -1;
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        subResult = deleteSpecifyDayAgo(buf, fileName, deleteDate);
                    else
                    {
                        strncpy(dateOfFileName, pDirStatus->d_name + fileNameLength, dateLength);
                        if ((strncmp(dateOfFileName, deleteDate, dateLength) <= 0) && (isAllNumber(dateOfFileName, dateLength)))
                        {
                            subResult = deleteFile(buf);
                        }
                        else
                            subResult = 0;
                    }
                }
                free(buf);
            }
            result = subResult;
        }
        closedir(pDir);
    }

    return result;
}

int32_t findOldestDate(IN const char *folderName, IN const char *fileName, IN OUT char *oldestDate)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    int fileNameLength = strlen(fileName);
    const int dateLength = 6;
    char dateOfFileName[dateLength + 1];
    char tempDate[dateLength + 1];
    int32_t result = -1;

    memset(dateOfFileName, '\0', dateLength + 1);
    memset(tempDate, 0xFF, dateLength + 1);

    if (pDir)
    {
        struct dirent *pDirStatus;

        result = 0;
        while (pDirStatus = readdir(pDir))
        {
            int32_t subResult = -1;
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        subResult = findOldestDate(buf, fileName, tempDate);
                    else
                    {
                        strncpy(dateOfFileName, pDirStatus->d_name + fileNameLength, dateLength);
                        if ((strncmp(dateOfFileName, tempDate, dateLength) <= 0) && (isAllNumber(dateOfFileName, dateLength)))
                        {
                            strncpy(tempDate, dateOfFileName, dateLength);
                            subResult = 0;
                        }
                    }
                }
                free(buf);
            }
            result = subResult;
        }
        closedir(pDir);
        if (strncmp(tempDate, oldestDate, dateLength) <= 0 || strlen(oldestDate) == 0)
        {
            strncpy(oldestDate, tempDate, dateLength);
        }
    }

    return result;
}

uint16_t createFolder(IN const char *folderName)
{
    int result = mkdir(folderName, 0777);
    return EXECUTE_OK;
}

int32_t deleteFolder(IN const char *folderName)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    int32_t result = -1;

    if (pDir)
    {
        struct dirent *pDirStatus;

        result = 0;
        while ((result == 0) && (pDirStatus = readdir(pDir)))
        {
            int32_t subResult = -1;
            char *buf;
            size_t len;

            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        subResult = deleteFolder(buf);
                    else
                        subResult = deleteFile(buf);
                }
                free(buf);
            }
            result = subResult;
        }
        closedir(pDir);
    }

    if (result == 0)
        result = rmdir(folderName);

    return result;
}

uint16_t writeFile(IN const char *path, IN const char *mode,
                   IN uint32_t offset, IN uint32_t origin, IN uint8_t *data, IN uint32_t dataLength)
{
    FILE *pFile;
    size_t ret;

    if ((data == NULL) || (mode == NULL))
    {
        return ERR_MISC_FILE_WRITE_ERROR;
    }

    pFile = fopen(path, mode);

    if (NULL == pFile)
    {
        return ERR_MISC_FILE_OPEN_ERROR;
    }

    ret = fseek(pFile, offset, origin);

    if (EXECUTE_OK != ret)
    {
        fclose(pFile);
        return ERR_MISC_FILE_SEEK_ERROR;
    }

    ret = fwrite(data, sizeof(unsigned char), dataLength, pFile);

    fflush(pFile);
    fsync(fileno(pFile));
    fclose(pFile);

    if (ret != dataLength)
    {
        return ERR_MISC_FILE_WRITE_ERROR;
    }

    return EXECUTE_OK;
}

uint16_t readFile(IN const char *path, IN const char *mode, IN uint32_t offset,
                  IN int origin, OUT uint8_t *data, IN OUT uint32_t *dataLength)
{
    IN FILE *pFile;
    size_t ret;

    if ((data == NULL) || (mode == NULL))
    {
        return ERR_MISC_FILE_READ_ERROR;
    }

    pFile = fopen(path, mode);
    if (NULL == pFile)
    {
        *dataLength = 0;
        return ERR_MISC_FILE_OPEN_ERROR;
    }

    ret = fseek(pFile, offset, origin);
    if (EXECUTE_OK != ret)
    {
        fclose(pFile);
        return ERR_MISC_FILE_SEEK_ERROR;
    }
    ret = fread(data, sizeof(unsigned char), *dataLength, pFile);

    fclose(pFile);

    *dataLength = ret;
    if (*dataLength == 0)
    {
        return ERR_MISC_FILE_READ_ERROR;
    }

    return EXECUTE_OK;
}

uint64_t getFileSize(IN const char *path)
{
    IN FILE *pFile;
    size_t ret;
    uint64_t size;

    pFile = fopen(path, "r");
    if (NULL == pFile)
    {
        return ERR_MISC_FILE_OPEN_ERROR;
    }

    ret = fseek(pFile, 0, SEEK_END);
    if (EXECUTE_OK != ret)
    {
        fclose(pFile);
        return ERR_MISC_FILE_SEEK_ERROR;
    }

    size = ftell(pFile);
    fclose(pFile);

    return size;
}

uint64_t getAllFileSize(IN const char *folderName)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    uint64_t size;

    if (pDir)
    {
        struct dirent *pDirStatus;
        size = 0;
        while (pDirStatus = readdir(pDir))
        {
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        size += getAllFileSize(buf);
                    else
                        size += getFileSize(buf);
                }
                free(buf);
            }
        }
        closedir(pDir);
    }

    return size;
}

uint64_t getSpecifyFileSize(IN const char *folderName, IN const char *fileName)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    uint64_t size;

    if (pDir)
    {
        struct dirent *pDirStatus;

        size = 0;
        while (pDirStatus = readdir(pDir))
        {
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        size += getSpecifyFileSize(buf, fileName);
                    else
                    {
                        if (strstr(pDirStatus->d_name, fileName) != NULL)
                            size += getFileSize(buf);
                    }
                }
                free(buf);
            }
        }
        closedir(pDir);
    }

    return size;
}

uint16_t checkFileExist(IN const char *folderName, IN const char *fileName, OUT char *existPath)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    uint16_t isExist = ERR_MISC_FILE_OPEN_ERROR;

    if (pDir)
    {
        struct dirent *pDirStatus;

        while ((isExist == ERR_MISC_FILE_OPEN_ERROR) && (pDirStatus = readdir(pDir)))
        {
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                    {
                        if (checkFileExist(buf, fileName, existPath) == EXECUTE_OK)
                            isExist = EXECUTE_OK;
                    }
                    else
                    {
                        if (strstr(pDirStatus->d_name, fileName) != NULL)
                        {
                            isExist = EXECUTE_OK;
                            memcpy(existPath, buf, len);
                        }
                    }
                }
                free(buf);
            }
        }
        closedir(pDir);
    }

    return isExist;
}

uint16_t splitPathIntoFolderAndFile(IN const char *path, OUT char *folder, IN uint32_t folderSize, OUT char *file, IN uint32_t fileSize)
{
    uint32_t tail = 0;
    uint32_t length = 0;
    int32_t i = 0;
    int32_t mode = 0;

    memset(folder, '\0', folderSize);
    memset(file, '\0', fileSize);
    tail = strlen(path);

    if (tail > folderSize + fileSize)
        return ERR_MISC_GET_DATA_FORMAT;

    for (i = tail; i > 0; i--)
    {
        if (path[i] == '/')
        {
            break;
        }
    }

    if (i != 0)
    {
        length = i;
        memcpy(folder, path, length);

        length = tail - (i + 1);
        memcpy(file, path + (i + 1), length);
    }
    else
    {
        memcpy(folder, "./", 2);
        memcpy(file, path, tail);
    }

    return EXECUTE_OK;
}

uint16_t getFileDateFromPath(IN const char *path, IN const char *fileName, OUT uint8_t *date)
{
    int16_t serialNumber = 0;
    int8_t *dateStartPosition = 0;
    uint16_t defineNameLenth = strlen(fileName);

    if (dateStartPosition = strstr(path, fileName))
    {
        memcpy(date, dateStartPosition + defineNameLenth, 6);
        return EXECUTE_OK;
    }

    return ERR_INPUT_FAIL;
}

int16_t getFileSerialNumberFromPath(IN const char *path, IN const char *date)
{
    int16_t serialNumber = 0;
    char *dateStartPosition = 0;
    uint16_t dateStringLenth = strlen(date);

    if (dateStartPosition = strstr(path, date))
    {
        serialNumber = (int16_t)(*(dateStartPosition + dateStringLenth) - '0');
    }

    if (serialNumber > 0 && serialNumber < 10)
        return serialNumber;

    return 0;
}

uint16_t getFileSerialNumberFromFolder(IN const char *folderName, IN const char *date)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    uint16_t fileSerialNumber = 0;
    uint16_t serialNumber = 0;

    if (pDir)
    {
        struct dirent *pDirStatus;

        while (pDirStatus = readdir(pDir))
        {
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        serialNumber = getFileSerialNumberFromFolder(buf, date);
                    else
                    {
                        fileSerialNumber = getFileSerialNumberFromPath(pDirStatus->d_name, date);
                        if (fileSerialNumber > serialNumber)
                            serialNumber = fileSerialNumber;
                    }
                }
                free(buf);
            }
        }
        closedir(pDir);
    }

    return serialNumber;
}

uint16_t isAllNumber(IN const char *string, IN int range)
{
    uint8_t result = 1;
    uint8_t i = 0;
    for (i = 0; i < range; i++)
    {
        // 0 - 9
        if ((string[i] < 48) || (string[i] > 57))
            result = 0;
    }
    return result;
}

uint16_t listAllFile(IN const char *folderName)
{
    DIR *pDir = opendir(folderName);
    size_t pathLen = strlen(folderName);
    uint16_t size;

    if (pDir)
    {
        struct dirent *pDirStatus;
        size = 0;
        while (pDirStatus = readdir(pDir))
        {
            char *buf;
            size_t len;
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(pDirStatus->d_name, ".") || !strcmp(pDirStatus->d_name, ".."))
                continue;

            len = pathLen + strlen(pDirStatus->d_name) + 2;
            buf = malloc(len);

            if (buf)
            {
                struct stat bufState;

                snprintf(buf, len, "%s/%s", folderName, pDirStatus->d_name);
                if (!stat(buf, &bufState))
                {
                    printf("%s\r\n", buf);
                    // determine the name is directory or not
                    if (S_ISDIR(bufState.st_mode))
                        listAllFile(buf);
                }
                free(buf);
            }
        }
        closedir(pDir);
    }

    return size;
}
