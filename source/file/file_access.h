#ifndef _FILE_ACCESS_H
#define _FILE_ACCESS_H

#include "common/custom_types.h"

#define FILE_MODE_R "rb"
#define FILE_MODE_RU "rb+"
#define FILE_MODE_W "wb"
#define FILE_MODE_WU "wb+"
#define FILE_MODE_A "ab"
#define FILE_MODE_AU "ab+"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

uint16_t createFile(IN const char *path);
uint16_t deleteFile(IN const char *path);
int32_t deleteSpecifyFile(IN const char *folderName, IN const char *deleteFileName);
int32_t deleteSpecifyDayAgo(IN const char *folderName, IN const char *fileName, IN const char *deleteDate);
int32_t findOldestDate(IN const char *folderName, IN const char *fileName, IN OUT char *oldestDate);
uint16_t createFolder(IN const char *folderName);
int32_t deleteFolder(IN const char *path);
uint16_t writeFile(IN const char *path, IN const char *mode,
                   IN uint32_t offset, IN uint32_t origin, IN uint8_t *data, IN uint32_t dataLength);
uint16_t readFile(IN const char *path, IN const char *mode,
                  IN uint32_t offset, IN int origin, OUT uint8_t *data, IN OUT uint32_t *dataLength);
uint64_t getFileSize(IN const char *path);
uint64_t getAllFileSize(IN const char *folderName);
uint64_t getSpecifyFileSize(IN const char *folderName, IN const char *fileName);
uint16_t checkFileExist(IN const char *folderName, IN const char *fileName, OUT char *existPath);
uint16_t splitPathIntoFolderAndFile(IN const char *path, OUT char *folder, IN uint32_t folderSize, OUT char *file, IN uint32_t fileSize);
uint16_t getFileDateFromPath(IN const char *path, IN const char *fileName, OUT uint8_t *date);
int16_t getFileSerialNumberFromPath(IN const char *path, IN const char *date);
uint16_t getFileSerialNumberFromFolder(IN const char *folderName, IN const char *date);
uint16_t isAllNumber(IN const char *string, IN int range);
uint16_t listAllFile(IN const char *folderName);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _FILE_ACCESS_H