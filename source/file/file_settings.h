#ifndef _FILE_SETTINGS_H
#define _FILE_SETTINGS_H

#include "common/custom_types.h"

#define FOLDER_NAME "test_log"
#define FILE_NAME "log"
#define FILE_EXTENSION "txt"
#define BACKUP_EXTENSION "backup"

#define PATH_MAX_LENGTH 100
#define NAME_MAX_LENGTH 40
#define EXTENSION_MAX_LENGTH 10
#define DATE_MAX_LENGTH 7
#define DATA_MAX_LENGTH 1000

#define ALL_FILE_MAX_SPACE 1000
#define ONE_FILE_MAX_SPACE 100
#define FILE_EXIST_DAY 3

typedef enum
{
    NO_SAVE_METHOD = 0,
    FOLDER_METHOD = 1,
    FILE_METHOD = 2
} saveFileMethod;

typedef struct
{
    uint8_t enable;
    uint64_t value;
} valueEnable;

typedef struct
{
    uint8_t enable;
    uint8_t ADYear[DATE_MAX_LENGTH];
} dateEnable;

typedef struct
{
    valueEnable allFileMax;
    valueEnable oneFileMax;
    valueEnable existDay;
} saveFileSpace;

typedef struct
{
    uint8_t enable;
    uint8_t extension[EXTENSION_MAX_LENGTH];
    uint8_t tempData[DATA_MAX_LENGTH];
} backupFile;

typedef struct
{
    uint8_t name[NAME_MAX_LENGTH];
    uint8_t extension[EXTENSION_MAX_LENGTH];
    dateEnable date;
    valueEnable serialNumber;
} baseFile;

typedef struct
{
    uint8_t path[PATH_MAX_LENGTH];
    uint8_t folderName[NAME_MAX_LENGTH];
    baseFile file;
    backupFile backup; //enable=1 : use backup file ; enable=0 : don't use backup file
    saveFileMethod saveMethod;
    saveFileSpace saveSpace;
} fileObject;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void initialFileObject(IN OUT fileObject *object);

const char *getPath(IN fileObject *object);
void setPath(IN fileObject *object, IN const char *path);

const char *getFolderName(IN fileObject *object);
void folderRename(IN OUT fileObject *object, IN const char *rename);

const char *getFileSurname(IN fileObject *object);
void fileRename(IN OUT fileObject *object, IN const char *rename);

const char *getFileExtension(IN fileObject *object);
void fileExtensionRename(IN OUT fileObject *object, IN const char *rename);

uint8_t isFileDateEnable(IN fileObject *object);
void enableFileDate(IN fileObject *object);
void disableFileDate(IN fileObject *object);
const char *getFileDate(IN fileObject *object);
void setFileDate(IN fileObject *object, IN const char *date);

uint8_t isFileSerialNumberEnable(IN fileObject *object);
void enableFileSerialNumber(IN fileObject *object);
void disableFileSerialNumber(IN fileObject *object);
uint32_t getFileSerialNumber(IN fileObject *object);
void setFileSerialNumber(IN fileObject *object, IN uint32_t value);

uint8_t isBackupEnable(IN fileObject *object);
void enableBackup(IN fileObject *object);
void disableBackup(IN fileObject *object);
const char *getBackupExtension(IN fileObject *object);
void backupExtensionRename(IN OUT fileObject *object, IN const char *rename);
char *getBackupTempData(IN fileObject *object);
void setBackupTempData(IN OUT fileObject *object, IN const char *tempData);

saveFileMethod getSaveMethod(IN fileObject *object);
void setSaveMethod(IN fileObject *object, IN saveFileMethod method);

uint8_t isAllFileSaveSpaceEnable(IN fileObject *object);
uint8_t enableAllFileSaveSpace(IN fileObject *object);
uint8_t disableAllFileSaveSpace(IN fileObject *object);
uint64_t getAllFileSaveSpace(IN fileObject *object);
void setAllFileMaxSpace(IN OUT fileObject *object, IN uint64_t maxSpace);

uint8_t isOneFileSaveSpaceEnable(IN fileObject *object);
uint8_t enableOneFileSaveSpace(IN fileObject *object);
uint8_t disableOneFileSaveSpace(IN fileObject *object);
uint64_t getOneFileSaveSpace(IN fileObject *object);
void setOneFileMaxSpace(IN OUT fileObject *object, IN uint64_t maxSpace);

uint8_t isExistDayEnable(IN fileObject *object);
uint8_t enableExistDay(IN fileObject *object);
uint8_t disableExistDay(IN fileObject *object);
uint64_t getExistDay(IN fileObject *object);
void setFileExistDay(IN OUT fileObject *object, IN uint64_t existDay);

void getFileFormatDate(char *date);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _FILE_SETTINGS_H