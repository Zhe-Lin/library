#ifndef _FILE_PROCESS_H
#define _FILE_PROCESS_H

#include "common/custom_types.h"
#include "file_settings.h"

#define FILE_KEY_NAME_DATE "\r\nDATE="
#define FILE_KEY_NAME_SN "\r\nSN="

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

uint16_t initialSaveMethod(IN OUT fileObject *object, IN saveFileMethod type);
uint16_t initialSavePath(IN OUT fileObject *object);
void getFileFullName(IN fileObject *object, OUT char *name);
uint16_t getFilePath(IN fileObject *object, OUT char *filePath);
uint16_t getBackupPath(IN fileObject *object, OUT char *backupPath);
uint16_t deleteFilePastDay(IN fileObject *object);
uint16_t deleteOldestFile(IN fileObject *object);
uint16_t checkOneFileSize(IN fileObject *object, IN uint64_t needSize);
uint16_t checkAllFileSize(IN fileObject *object);
uint16_t writeBackupToTempData(IN fileObject *object, IN const char *format, ...);
uint16_t updateBackupFileFromTempData(IN fileObject *object);
uint16_t restoreDataFromBackup(IN fileObject *object, IN const char *key, OUT char *value);
uint16_t initialFileDate(OUT fileObject *object);
uint16_t initialFileSerialNumber(IN OUT fileObject *object);
uint16_t rollSerialNumber(IN OUT fileObject *object);
uint16_t checkDate(IN OUT fileObject *object);
void createLog(IN OUT fileObject *object);
uint16_t writeProcess(IN OUT fileObject *object, IN OUT char *mode, IN int seek, IN uint8_t *data, IN uint32_t datalen);
char filterChar(char aChar);
const char *filterPath(char *path);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _FILE_PROCESS_H