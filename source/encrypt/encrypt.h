#ifndef _ENCRYPT_H
#define _ENCRYPT_H

#include "common/custom_types.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    uint8_t crc8(uint8_t *data, uint16_t length);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _ENCRYPT_H