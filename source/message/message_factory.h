#ifndef _MESSAGE_FACTORY_H
#define _MESSAGE_FACTORY_H

#include "common/custom_types.h"

typedef struct IMESSAGE IMessage;

typedef struct MESSAGE_COMPONENTS
{
    uint8_t number;
    IMessage **binding;
} messageComponents;

typedef enum PROCESS_STATE
{
    stateUndo = 0,
    stateDoing,
    stateDone,
} processState;

typedef enum MESSAGE_TYPE
{
    typeValue = 0,
    typeString,
} messageType;

typedef struct IMESSAGE
{
    bool (*process)(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers);
    messageType type;
    uint8_t name[10];
    uint32_t size;
    processState state;
} IMessage;

typedef struct MESSAGE_VALUE
{
    bool (*process)(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers);
    messageType type;
    uint8_t name[10];
    uint32_t size;
    processState state;

    uint8_t value;
} messageValue;

typedef struct MESSAGE_STRING
{
    bool (*process)(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers);
    messageType type;
    uint8_t name[10];
    uint32_t size;
    processState state;

    char *string;
    uint8_t stringNumber;
} messageString;

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    messageComponents *makeComponents(IN uint8_t number);
    void freeComponent(OUT messageComponents *components);
    messageValue *makeValue(IN const char *name, IN uint8_t value);
    messageString *makeString(IN const char *name, IN const char *string, IN uint8_t stringNumber);
    void freeString(OUT messageComponents *components, IN uint32_t index);
    uint32_t getComponentIndex(IN messageComponents *components, IN const char *name);
    bool setComponentSize(OUT messageComponents *components, IN const char *name, IN uint32_t size);
    uint32_t getComponentSize(IN messageComponents *components, IN const char *name);
    bool setComponentState(OUT messageComponents *components, IN uint32_t index, processState state);
    processState getComponentState(OUT messageComponents *components, IN uint32_t index);
    bool executeComponentProcess(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MESSAGE_FACTORY_H