#ifndef _MESSAGE_APPLICATION_H
#define _MESSAGE_APPLICATION_H

#include "common/custom_types.h"
#include "third_party/cJSON.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    void registerComponent(void);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MESSAGE_APPLICATION_H