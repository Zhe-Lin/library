#include <stdio.h>
#include <string.h>

#include "message_check.h"
#include "encrypt/encrypt.h"

IMessage *bindingCRC(IN const char *name, IN const char *specifyCheckSplitBySpace, uint8_t specifyCheckNumber)
{
    messageString *object = makeString(name, specifyCheckSplitBySpace, specifyCheckNumber);

    object->process = processCRC;

    return (IMessage *)object;
}

bool processCRC(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers)
{
    messageString *object = (messageString *)components->binding[index];

    const char *splitSign = " ,";
    char *token;
    char buffer[100] = {'\0'};
    char mockBuffer[] = {0xE1, 0xA2};
    int offset = 0;

    // printf("specifyCheckbuffer:%d\r\n", object->stringNumber);
    // printf("specifyCheckbuffer:%s\r\n", object->string);

    token = strtok(object->string, splitSign);

    for (int i = 0; i < object->stringNumber; i++)
    {
        memcpy(buffer + offset, mockBuffer, sizeof(mockBuffer));
        offset += sizeof(mockBuffer);

        printf("%s\r\n", token);
        token = strtok(NULL, " ");
    }

    // for (int i = 0; i < strlen(buffer); i++)
    //     printf("%x ", buffer[i]);
    // printf("\r\n");

    // if (*data != object->value)
    // {
    //     return false;
    // }

    return true;
}
