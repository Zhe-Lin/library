#ifndef _MESSAGE_RECORD_H
#define _MESSAGE_RECORD_H

#include "common/custom_types.h"
#include "message_cJSON.h"
#include "message_factory.h"

typedef enum USE_METHOD
{
    directSave = 0,
    noRepeatSave,
} useMethod;

typedef struct
{
    cJSON *cjsonMain;
    cJSON *cjsonSub;
    uint8_t id;
    uint8_t componentsIndex;
    char *componentsRepeatCommand;
    useMethod method;
} messageQueue;

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    void initialQueue(OUT messageQueue *queue);
    void freeQueue(OUT messageQueue *queue);
    void setQueueUseMethod(OUT messageQueue *queue, IN useMethod method, IN char *specifyComponent);
    void resetReference(OUT messageQueue *queue);
    char *getIdName(OUT messageQueue *queue);
    void nextQueueId(OUT messageQueue *queue);
    cJSON *getSpecifyId(IN messageQueue *queue, IN uint8_t id);
    void setIdIsUsingById(IN messageQueue *queue, IN uint8_t id);
    void setIdIsUsingByObject(IN messageQueue *queue, IN cJSON *object);
    void setIdNotUsingById(IN messageQueue *queue, IN uint8_t id);
    void setIdNotUsingByObject(IN messageQueue *queue, IN cJSON *object);
    bool getIdUsingStatusById(IN messageQueue *queue, IN uint8_t id);
    bool getIdUsingStatusByObject(IN messageQueue *queue, IN cJSON *object);
    uint32_t getSpecifyArraySize(IN messageQueue *queue, IN uint8_t id, IN char *name);
    void getSpecifyArrayData(IN messageQueue *queue, IN uint8_t id, IN char *name, OUT unsigned char *data);
    cJSON *getRepeatCommand(IN messageQueue *queue, IN unsigned char *data);
    void updateSaveNode(OUT messageQueue *queue, IN unsigned char *data);
    void saveData(OUT messageQueue *queue, IN messageComponents *components, IN char *data, IN uint32_t dataSize);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MESSAGE_RECORD_H