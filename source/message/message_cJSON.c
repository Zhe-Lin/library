#include <stdio.h>
#include <stdlib.h>

#include "message_cJSON.h"

int32_t cJSON_getItemNumber(IN cJSON *object, IN const char *itemString)
{
    cJSON *item = cJSON_GetObjectItem(object, itemString);
    return (NULL != item) ? (int32_t)(item->valueint) : 0;
}

char *cJSON_getItemString(IN cJSON *object, IN const char *itemString)
{
    cJSON *item = cJSON_GetObjectItem(object, itemString);
    return (NULL != item) ? (char *)(item->valuestring) : (NULL);
}

void cJSON_resetItemNumber(OUT cJSON *object, IN const char *itemString, IN int32_t valueNumber)
{
    cJSON *item = cJSON_GetObjectItem(object, itemString);
    cJSON_SetNumberValue(item, valueNumber);
}

void cJSON_resetItemString(OUT cJSON *object, IN const char *itemString, IN const char *resetString)
{
    cJSON *item = cJSON_GetObjectItem(object, itemString);
    cJSON_SetValuestring(item, resetString);
}

void cJSON_printAndFree(IN cJSON *object)
{
    char *text = NULL;

    text = cJSON_Print(object);
    printf("%s", text);

    free(text);
}
