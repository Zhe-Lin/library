#include <stdio.h>

#include "message_head.h"

IMessage *bindingHead(IN const char *name, IN uint8_t head)
{
    messageValue *object = makeValue(name, head);

    object->process = processHead;

    return (IMessage *)object;
}

bool processHead(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers)
{
    messageValue *object = (messageValue *)components->binding[index];

    if (*data != object->value)
    {
        return false;
    }

    return true;
}
