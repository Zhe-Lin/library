#ifndef _MESSAGE_HEAD_H
#define _MESSAGE_HEAD_H

#include "common/custom_types.h"
#include "message_factory.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    IMessage *bindingHead(IN const char *name, IN uint8_t head);
    bool processHead(IN messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MESSAGE_HEAD_H