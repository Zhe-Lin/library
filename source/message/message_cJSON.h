#ifndef _MESSAGE_CJSON_H
#define _MESSAGE_CJSON_H

#include "common/custom_types.h"
#include "third_party/cJSON.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    int32_t cJSON_getItemNumber(IN cJSON *object, IN const char *itemString);
    char *cJSON_getItemString(IN cJSON *object, IN const char *itemString);
    void cJSON_resetItemNumber(OUT cJSON *object, IN const char *itemString, IN int32_t valueNumber);
    void cJSON_resetItemString(OUT cJSON *object, IN const char *itemString, IN const char *resetString);
    void cJSON_printAndFree(IN cJSON *object);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MESSAGE_CJSON_H