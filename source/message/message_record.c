#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "message_record.h"
#include "message_factory.h"
#include "transform/transform_ascii.h"

void initialQueue(OUT messageQueue *queue)
{
    queue->cjsonMain = cJSON_CreateObject();
    queue->cjsonSub = NULL;
    queue->id = 0;
    queue->componentsIndex = 0;
    queue->method = directSave;
    queue->componentsRepeatCommand = NULL;
}

void setQueueUseMethod(OUT messageQueue *queue, IN useMethod method, IN char *specifyComponent)
{
    static char repeatComponent[10] = {'\0'};
    strcpy(repeatComponent, specifyComponent);

    switch (method)
    {
    case noRepeatSave:
        queue->method = noRepeatSave;
        queue->componentsRepeatCommand = repeatComponent;
        break;

    default:
        queue->method = directSave;
        queue->componentsRepeatCommand = NULL;
        break;
    }
}

void freeQueue(OUT messageQueue *queue)
{
    cJSON_Delete(queue->cjsonMain);
}

void resetReference(OUT messageQueue *queue)
{
    queue->componentsIndex = 0;
    queue->cjsonSub = NULL;
}

char *getIdName(OUT messageQueue *queue)
{
    static char idName[10] = {'\0'};
    sprintf(idName, "id_%d", queue->id);
    return idName;
}

void nextComponentsIndex(OUT messageQueue *queue, IN messageComponents *components)
{
    if (getComponentState(components, queue->componentsIndex) == stateDone)
    {
        setComponentState(components, queue->componentsIndex, stateUndo);
        queue->componentsIndex += 1;
    }
}

void nextQueueId(OUT messageQueue *queue)
{
    char idName[10] = {'\0'};

    sprintf(idName, "id_%d", queue->id);

    if (cJSON_GetObjectItem(queue->cjsonMain, idName) != NULL)
    {
        queue->id += 1;
    }
}

bool isFullCommand(IN messageQueue *queue, IN messageComponents *components)
{
    if (queue->componentsIndex >= components->number)
        return true;

    return false;
}

cJSON *getSpecifyId(IN messageQueue *queue, IN uint8_t id)
{
    char idName[10] = {'\0'};

    sprintf(idName, "id_%d", id);

    return cJSON_GetObjectItem(queue->cjsonMain, idName);
}

void setIdIsUsingById(IN messageQueue *queue, IN uint8_t id)
{
    cJSON *object = getSpecifyId(queue, id);

    if (cJSON_GetObjectItem(object, "using") != NULL)
    {
        cJSON_DeleteItemFromObject(object, "using");
    }

    cJSON_AddTrueToObject(object, "using");
}

void setIdIsUsingByObject(IN messageQueue *queue, IN cJSON *object)
{
    if (cJSON_GetObjectItem(object, "using") != NULL)
    {
        cJSON_DeleteItemFromObject(object, "using");
    }

    cJSON_AddTrueToObject(object, "using");
}

void setIdNotUsingById(IN messageQueue *queue, IN uint8_t id)
{
    cJSON *object = getSpecifyId(queue, id);

    if (cJSON_GetObjectItem(object, "using") != NULL)
    {
        cJSON_DeleteItemFromObject(object, "using");
    }

    cJSON_AddFalseToObject(object, "using");
}

void setIdNotUsingByObject(IN messageQueue *queue, IN cJSON *object)
{
    if (cJSON_GetObjectItem(object, "using") != NULL)
    {
        cJSON_DeleteItemFromObject(object, "using");
    }

    cJSON_AddFalseToObject(object, "using");
}

bool getIdUsingStatusById(IN messageQueue *queue, IN uint8_t id)
{
    return cJSON_IsTrue(cJSON_GetObjectItem(getSpecifyId(queue, id), "using"));
}

bool getIdUsingStatusByObject(IN messageQueue *queue, IN cJSON *object)
{
    return cJSON_IsTrue(cJSON_GetObjectItem(object, "using"));
}

cJSON *getSpecifyArray(IN messageQueue *queue, IN uint8_t id, IN char *name)
{
    char idName[10] = {'\0'};
    cJSON *idItem = NULL;
    cJSON *specifyItem = NULL;

    sprintf(idName, "id_%d", id);
    idItem = cJSON_GetObjectItem(queue->cjsonMain, idName);
    specifyItem = cJSON_GetObjectItem(idItem, name);

    return specifyItem;
}

uint32_t getSpecifyArraySize(IN messageQueue *queue, IN uint8_t id, IN char *name)
{
    cJSON *specifyItem = getSpecifyArray(queue, id, name);

    return cJSON_GetArraySize(specifyItem);
}

void getSpecifyArrayData(IN messageQueue *queue, IN uint8_t id, IN char *name, OUT unsigned char *data)
{
    cJSON *specifyItem = getSpecifyArray(queue, id, name);
    uint32_t itemSize = cJSON_GetArraySize(specifyItem);
    cJSON *itemIndex = NULL;

    for (uint32_t i = 0; i < itemSize; i++)
    {
        itemIndex = cJSON_GetArrayItem(specifyItem, i);
        data[i] = charByteToHexByte(itemIndex->valuestring);
    }
}

cJSON *getRepeatCommand(IN messageQueue *queue, IN unsigned char *data)
{
    uint32_t arraySize = getSpecifyArraySize(queue, 0, queue->componentsRepeatCommand);
    unsigned char arrayData[arraySize];
    memset(arrayData, '\0', arraySize);

    for (uint8_t id = 0; id <= queue->id; id++)
    {
        getSpecifyArrayData(queue, id, queue->componentsRepeatCommand, arrayData);

        if (!strncasecmp(arrayData, data, arraySize))
        {
            return getSpecifyId(queue, id);
        }
    }

    return NULL;
}

void updateSaveNode(OUT messageQueue *queue, IN unsigned char *data)
{
    cJSON *specifyNode = NULL;

    if (queue->method == noRepeatSave)
    {
        specifyNode = getRepeatCommand(queue, data);
        if (specifyNode != NULL)
        {
            if (getIdUsingStatusByObject(queue, specifyNode) == false)
            {
                queue->cjsonSub = specifyNode;
                return;
            }
        }
    }

    if (queue->cjsonSub == NULL)
    {
        queue->cjsonSub = cJSON_CreateObject();
        cJSON_AddItemToObject(queue->cjsonMain, getIdName(queue), queue->cjsonSub);
    }
}

void saveData(OUT messageQueue *queue, IN messageComponents *components, IN char *data, IN uint32_t dataSize)
{
    cJSON *cjsonData = cJSON_CreateArray();
    cJSON *cjsonChange = queue->cjsonSub;

    updateSaveNode(queue, data);

    if (cjsonChange != queue->cjsonSub && cjsonChange != NULL)
    {
        // have repeat command. delete redundant index.
        cJSON_DeleteItemFromObject(queue->cjsonMain, cjsonChange->string);
    }

    if (cJSON_GetObjectItem(queue->cjsonSub, components->binding[queue->componentsIndex]->name) != NULL)
    {
        // have repeat command. delete repeat item.
        cJSON_DeleteItemFromObject(queue->cjsonSub, components->binding[queue->componentsIndex]->name);
    }

    cJSON_AddItemToObject(queue->cjsonSub, components->binding[queue->componentsIndex]->name, cjsonData);
    for (uint32_t i = 0; i < dataSize; i++)
    {
        cJSON_AddItemToArray(cjsonData, cJSON_CreateString(hexByteToCharByte(data[i])));
    }
}

void queue(messageQueue *queue, messageComponents *components, char *data, uint32_t dataSize)
{
    if (executeComponentProcess(components, queue->componentsIndex, data, dataSize) == false)
    {
        resetReference(queue);
        return;
    }

    saveData(queue, components, data, dataSize);

    nextComponentsIndex(queue, components);
    nextQueueId(queue);
    if (isFullCommand(queue, components) == true)
    {
        // notifyPop();
        resetReference(queue);
    }
}
