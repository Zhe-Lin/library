#ifndef _TIME_MOCK_H
#define _TIME_MOCK_H

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>

#include "time/time_access.h"
using trompeloeil::_;

class STUB_DATE
{
  public:
    MAKE_MOCK2(getCurrentDate, void(char *, uint32_t));
};

extern STUB_DATE stubDate;

#endif // _TIME_MOCK_H