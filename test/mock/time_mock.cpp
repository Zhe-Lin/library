#include "time_mock.hpp"
#include <stdio.h>

STUB_DATE stubDate;

extern "C" {
void getCurrentDate(IN OUT char *date, IN uint32_t size)
{
    return stubDate.getCurrentDate(date, size);
}
}
