#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message/message_factory.h"
#include "message/message_head.h"

using Catch::Matchers::Equals;

SCENARIO("component set and get")
{
    GIVEN("create a component object and binding head")
    {
        int32_t result = 0xFFFF;
        messageComponents *Components = makeComponents(3);

        Components->binding[0] = bindingHead("HEAD", 0xE8);
        Components->binding[1] = bindingHead("HCRC", 0xE8);
        Components->binding[2] = bindingHead("CMD", 0xE8);

        WHEN("get component index")
        {
            result = getComponentIndex(Components, "HCRC");

            THEN("result is 1")
            {
                CHECK(result == 1);
            }
        }

        WHEN("set component size")
        {
            result = setComponentSize(Components, "HCRC", 15);

            THEN("result is true (success)")
            {
                CHECK(result == true);
            }
        }

        WHEN("get component size")
        {
            setComponentSize(Components, "HCRC", 15);
            result = getComponentSize(Components, "HCRC");

            THEN("result is 15")
            {
                CHECK(result == 15);
            }
        }

        freeComponent(Components);
    }
}
