#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message/message_record.h"
#include "message/message_head.h"

using Catch::Matchers::Equals;

SCENARIO("process queue")
{
    GIVEN("initial queue,and set queue method is noRepeatSave")
    {
        int32_t result = false;
        messageQueue queue;
        initialQueue(&queue);
        setQueueUseMethod(&queue, noRepeatSave, (char *)"CMD");

        WHEN("get queue method")
        {
            result = queue.method;

            THEN("result is noRepeatSave")
            {
                CHECK(result == noRepeatSave);
            }
        }

        freeQueue(&queue);
    }

    GIVEN("initial queue")
    {
        std::string result;
        messageQueue queue;
        initialQueue(&queue);

        WHEN("set queue id is 5,and get id name")
        {
            queue.id = 5;

            THEN("get id name, result is id_5")
            {
                result = getIdName(&queue);
                CHECK_THAT(result, Equals("id_5"));
            }
        }

        WHEN("set queue id is 2,and get id name")
        {
            queue.id = 2;

            THEN("get id name, result is id_2")
            {
                result = getIdName(&queue);
                CHECK_THAT(result, Equals("id_2"));
            }
        }

        freeQueue(&queue);
    }

    GIVEN("initial queue and make 3 components")
    {

        uint8_t fackHead[] = {0xE8};
        uint8_t fackCmd[1];
        uint8_t fackData[] = {0x3A, 0x5E, 0xA2, 0x0D, 0x0B};

        messageComponents *components = makeComponents(3);
        components->binding[0] = bindingHead("HEAD", 0xE8);
        components->binding[1] = bindingHead("CMD", 0);
        components->binding[2] = bindingHead("DATA", 0);

        messageQueue queue;
        initialQueue(&queue);

        queue.id = 0;
        queue.componentsIndex = 0;
        saveData(&queue, components, (char *)fackHead, 1);
        queue.componentsIndex = 1;
        fackCmd[0] = 0xA1;
        saveData(&queue, components, (char *)fackCmd, 1);
        queue.componentsIndex = 2;
        setComponentSize(components, "DATA", 5);
        fackData[2] = 0xA3;
        saveData(&queue, components, (char *)fackData, 5);
        setIdNotUsingByObject(&queue, queue.cjsonSub);
        resetReference(&queue);

        queue.id = 1;
        queue.componentsIndex = 0;
        saveData(&queue, components, (char *)fackHead, 1);
        queue.componentsIndex = 1;
        fackCmd[0] = 0x3B;
        saveData(&queue, components, (char *)fackCmd, 1);
        queue.componentsIndex = 2;
        setComponentSize(components, "DATA", 5);
        fackData[2] = 0xEB;
        saveData(&queue, components, (char *)fackData, 5);
        setIdIsUsingByObject(&queue, queue.cjsonSub);
        resetReference(&queue);

        queue.id = 2;
        queue.componentsIndex = 0;
        saveData(&queue, components, (char *)fackHead, 1);
        queue.componentsIndex = 1;
        fackCmd[0] = 0x0F;
        saveData(&queue, components, (char *)fackCmd, 1);
        queue.componentsIndex = 2;
        setComponentSize(components, "DATA", 5);
        fackData[2] = 0x6E;
        saveData(&queue, components, (char *)fackData, 5);
        setIdNotUsingByObject(&queue, queue.cjsonSub);
        resetReference(&queue);

        WHEN("get CMD data")
        {
            cJSON *cjson_array = NULL;
            uint32_t arraySize = getSpecifyArraySize(&queue, 1, (char *)"CMD");
            unsigned char data[arraySize];
            int32_t result;

            getSpecifyArrayData(&queue, 1, (char *)"CMD", data);
            result = data[0];

            THEN("result is 0x3B")
            {
                CHECK(result == 0x3B);
            }
        }

        WHEN("get id:1 using status by id")
        {
            bool result = false;

            result = getIdUsingStatusById(&queue, 1);

            THEN("result is true")
            {
                CHECK(result == true);
            }
        }

        WHEN("get id:1 using status by object")
        {
            bool result = false;

            result = getIdUsingStatusByObject(&queue, getSpecifyId(&queue, 1));

            THEN("result is true")
            {
                CHECK(result == true);
            }
        }

        WHEN("id current is 2, and queue need next id")
        {
            int result = 0;

            queue.id = 2;
            nextQueueId(&queue);

            result = queue.id;

            THEN("result is 3")
            {
                CHECK(result == 3);
            }
        }

        WHEN("id current is 3, and queue not need next id")
        {
            int result = 0;

            queue.id = 3;
            nextQueueId(&queue);

            result = queue.id;

            THEN("result is 3")
            {
                CHECK(result == 3);
            }
        }

        WHEN("get repeat command")
        {
            uint8_t command[] = {0x3B};
            queue.componentsRepeatCommand = (char *)"CMD";
            cJSON *result = getRepeatCommand(&queue, command);

            THEN("result is id_1")
            {
                CHECK_THAT(result->string, Equals("id_1"));
            }
        }

        WHEN("get repeat data")
        {
            uint8_t command[] = {0x3A, 0x5E, 0x6E, 0x0D, 0x0B};
            queue.componentsRepeatCommand = (char *)"DATA";
            cJSON *result = getRepeatCommand(&queue, command);

            THEN("result is id_2")
            {
                CHECK_THAT(result->string, Equals("id_2"));
            }
        }

        WHEN("set repeat command is 0x0F, and get repeat command node")
        {
            std::string result;
            uint8_t command[] = {0x0F};
            setQueueUseMethod(&queue, noRepeatSave, (char *)"CMD");
            queue.id = 3;
            updateSaveNode(&queue, command);
            result = queue.cjsonSub->string;

            THEN("repeat command node is exist, so result is id_2")
            {
                CHECK_THAT(result, Equals("id_2"));
            }
        }

        WHEN("set repeat command is 0x3B, and get repeat command node")
        {
            std::string result;
            uint8_t command[] = {0x3B};
            setQueueUseMethod(&queue, noRepeatSave, (char *)"CMD");
            queue.id = 3;
            updateSaveNode(&queue, command);
            result = queue.cjsonSub->string;

            THEN("repeat command node is using, so result is id_3, replace with id_1")
            {
                CHECK_THAT(result, Equals("id_3"));
            }
        }

        WHEN("set repeat command is 0x0F, and save new data")
        {
            cJSON_printAndFree(queue.cjsonMain);
            printf("\r\n");

            uint8_t getdata[5] = {'\0'};
            uint8_t data[] = {0x4B, 0x6F, 0xB3, 0x0E, 0x0A};
            uint8_t command[] = {0x0F};
            setQueueUseMethod(&queue, noRepeatSave, (char *)"CMD");

            queue.id = 3;
            queue.componentsIndex = 0;
            saveData(&queue, components, (char *)fackHead, 1);
            queue.componentsIndex = 1;
            fackCmd[0] = 0x0F;
            saveData(&queue, components, (char *)command, 1);
            queue.componentsIndex = 2;
            setComponentSize(components, "DATA", 5);
            fackData[2] = 0x6E;
            saveData(&queue, components, (char *)data, 5);
            setIdNotUsingByObject(&queue, queue.cjsonSub);
            resetReference(&queue);

            getSpecifyArrayData(&queue, 2, (char *)"DATA", getdata);

            THEN("get data from new cjson table, ensure data is correct. result is 0.")
            {
                int result = strncasecmp((char *)getdata, (char *)data, 5);
                CHECK(result == 0);
            }

            printf("=========== after save ===========\r\n");
            cJSON_printAndFree(queue.cjsonMain);
            printf("\r\n");
        }

        freeQueue(&queue);
        freeComponent(components);
    }
}
