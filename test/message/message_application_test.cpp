#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message/message_application.h"

using Catch::Matchers::Equals;

// SCENARIO("message base object")
// {
//     GIVEN("create a cJSON object")
//     {
//         messageQueue queue;
//         int32_t result = 0xFFFF;

//         initialQueue(&queue);

//         WHEN("get initial id")
//         {
//             result = getNumber(queue.cjson, "id");

//             THEN("id is 0")
//             {
//                 CHECK(result == 0);
//             }
//         }
//     }
// }
