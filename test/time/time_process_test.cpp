#include <string.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "time/time_process.h"
#include "mock/time_mock.hpp"

using Catch::Matchers::Equals;

SCENARIO("offset date")
{
    GIVEN("the date is '20210309'")
    {
        std::string result;
        char date[] = "20210309";

        WHEN("offset date before 10 day")
        {
            offsetDate(date, -10);
            THEN("result is '20210227'")
            {
                result = date;
                CHECK_THAT(result, Equals("20210227"));
            }
        }
    }

    GIVEN("the date is '20210227'")
    {
        std::string result;
        char date[] = "20210227";

        WHEN("offset date before 10 day")
        {
            offsetDate(date, 10);
            THEN("result is '20210309'")
            {
                result = date;
                CHECK_THAT(result, Equals("20210309"));
            }
        }
    }
}
