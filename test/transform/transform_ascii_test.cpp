#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "transform/transform_ascii.h"

using Catch::Matchers::Equals;

SCENARIO("transform hex to char")
{
    GIVEN("create a hex code is 0xA5")
    {
        uint8_t hex = 0xA5;

        WHEN("transform code")
        {
            std::string result;

            result = hexByteToCharByte(hex);

            THEN("result is 'A5'")
            {
                CHECK_THAT(result, Equals("A5"));
            }
        }
    }

    GIVEN("create a hex code is 0x0f")
    {
        uint8_t hex = 0x0f;

        WHEN("transform code")
        {
            std::string result;

            result = hexByteToCharByte(hex);

            THEN("result is '0F'")
            {
                CHECK_THAT(result, Equals("0F"));
            }
        }
    }
}

SCENARIO("transform char to hex")
{
    GIVEN("create a char is 'A5'")
    {
        char charByte[] = "A5";

        WHEN("transform code")
        {
            unsigned char result;

            result = charByteToHexByte(charByte);

            THEN("result is 0xA5")
            {
                CHECK(result == 0xA5);
            }
        }
    }

    GIVEN("create a char is '0f'")
    {
        char charByte[] = "0f";

        WHEN("transform code")
        {
            unsigned char result;

            result = charByteToHexByte(charByte);

            THEN("result is 0x0F")
            {
                CHECK(result == 0x0F);
            }
        }
    }
}
