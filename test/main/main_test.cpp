/**
 ******************************************************************************
 * File Name          : main_test.cpp
 * Description        : Main program body
 ******************************************************************************
 *
 *
 ******************************************************************************
 */
// Includes ------------------------------------------------------------------
#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch_all.hpp>

// Internal variables ---------------------------------------------------------

// Extern variables -----------------------------------------------------------

// Function--------------------------------------------------------------------
int main(int argc, char *argv[])
{
    // global setup...

    int result = Catch::Session().run(argc, argv);

    // global clean-up...

    return result;
}
