#include <string.h>
#include <unistd.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "common/custom_errors.h"
#include "file/file_access.h"

using Catch::Matchers::Equals;

SCENARIO("create file")
{
    GIVEN("folder no exist")
    {
        int32_t result = 0xFFFF;

        WHEN("create file should fail")
        {
            result = createFile("testlog/log1.txt");

            THEN("result is ERR_MISC_FILE_OPEN_ERROR")
            {
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }
        }
    }
    GIVEN("folder exist")
    {
        int32_t result = 0xFFFF;
        createFolder("testlog");

        WHEN("create file should fail")
        {
            result = createFile("testlog/log1.txt");

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }

        deleteFolder("testlog");
    }
}

SCENARIO("delete folder and file")
{
    GIVEN("create a folder & 3 files")
    {
        int32_t result = 0xFFFF;

        createFolder("testlog");
        createFile("testlog/log1.txt");
        createFile("testlog/log2.txt");
        createFile("testlog/log3.txt");

        WHEN("delete folder,and folder's file")
        {
            result = deleteFolder("testlog");

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }
    }
}

SCENARIO("delete specify file")
{
    GIVEN("create a folder & 3 files")
    {
        int32_t result = 0xFFFF;

        createFolder("testlog");
        createFile("testlog/log1.txt");
        createFile("testlog/log2.txt");
        createFile("testlog/log3.txt");

        WHEN("delete folder,and folder's file")
        {
            result = deleteSpecifyFile("testlog", "log");
            result = rmdir("testlog");

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }
    }
}

SCENARIO("delete specify file day ago")
{
    GIVEN("create two folder & 7 files")
    {
        int32_t result = 0xFFFF;
        char existPath[50] = {'\0'};

        createFolder("testlog");
        createFile("testlog/log210711.txt");
        createFile("testlog/log210712.txt");
        createFile("testlog/log210713.txt");
        createFile("testlog/log210714.txt");
        createFile("testlog/log210715.txt");

        createFolder("testlog/testlog2");
        createFile("testlog/testlog2/log210712.txt");
        createFile("testlog/testlog2/log210715.txt");

        WHEN("delete file that 210713 ago")
        {
            result = deleteSpecifyDayAgo("testlog", "log", "210713");

            THEN("'testlog/log210711.txt' is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog", "log210711", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("'testlog/log210712.txt' is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog", "log210712", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("'testlog/log210713.txt' is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog", "log210713", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("'testlog/testlog2/log210712.txt' is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog/testlog2", "log210712", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("'testlog/log210714.txt' is EXECUTE_OK")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog", "log210714", existPath);
                CHECK(result == EXECUTE_OK);
            }

            THEN("'testlog/log210715.txt' is EXECUTE_OK")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog", "log210715", existPath);
                CHECK(result == EXECUTE_OK);
            }

            THEN("'testlog/testlog2/log210715.txt' is EXECUTE_OK")
            {
                memset(existPath, '\0', 50);
                result = checkFileExist("testlog/testlog2", "log210715", existPath);
                CHECK(result == EXECUTE_OK);
            }

            deleteFolder("testlog");
        }
    }
}

SCENARIO("find oldest date")
{
    GIVEN("create two folder & 5 files")
    {
        std::string result;
        char date[7] = {'\0'};

        createFolder("testlog");
        createFile("testlog/log210709.txt");
        createFile("testlog/log210712.txt");
        createFile("testlog/log210716.txt");

        createFolder("testlog/testlog2");
        createFile("testlog/testlog2/log210713.txt");
        createFile("testlog/testlog2/log210715.txt");

        WHEN("find oldest date")
        {
            findOldestDate("testlog", "log", date);

            THEN("result is ERR_MISC_FILE_OPEN_ERROR")
            {
                result = date;
                CHECK_THAT(result, Equals("210709"));
            }

            deleteFolder("testlog");
        }
    }
}

SCENARIO("write file")
{
    GIVEN("a string='123456789' & length=9")
    {
        FILE *pFile;
        uint16_t result = 0xFFFF;
        uint8_t data[] = "123456789";

        WHEN("write string to test_log.txt")
        {
            result = writeFile("test_log.txt", FILE_MODE_AU, 0x00, SEEK_END, data, sizeof(data));

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
            deleteFile("test_log.txt");
        }

        WHEN("write string to log/test_log.txt, but log folder not exist")
        {
            result = writeFile("log/test_log.txt", FILE_MODE_AU, 0x00, SEEK_END, data, sizeof(data));

            THEN("result is ERR_MISC_FILE_OPEN_ERROR")
            {
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }
        }

        WHEN("write string to log/test_log.txt, and log folder exist")
        {
            createFolder("log");
            result = writeFile("log/test_log.txt", FILE_MODE_AU, 0x00, SEEK_END, data, sizeof(data));

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
            deleteFolder("log");
        }
    }
}

SCENARIO("read file")
{
    GIVEN("a string='123456789' & length=9")
    {
        FILE *pFile;
        std::string result;
        uint8_t data[] = "123456789";
        uint32_t dataLength = 0;

        WHEN("write string to test_log.txt,and read test_log.txt")
        {
            writeFile("test_log.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));

            memset(data, '\0', sizeof(data));
            dataLength = sizeof(data);
            readFile("test_log.txt", FILE_MODE_R, 0, SEEK_SET, data, &dataLength);

            THEN("result is EXECUTE_OK")
            {
                result = (char *)data;
                CHECK_THAT(result, Equals("123456789"));
            }
            deleteFile("test_log.txt");
        }
    }
}

SCENARIO("get file size")
{
    GIVEN("a string='123456789'")
    {
        FILE *pFile;
        uint16_t result = 0xFFFF;
        uint8_t data[] = "123456789";

        WHEN("write string to test_log.txt,and read test_log.txt")
        {
            writeFile("test_log.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
            result = getFileSize("test_log.txt");

            THEN("result is data size")
            {
                CHECK(result == sizeof(data));
            }
            deleteFile("test_log.txt");
        }
    }
}

SCENARIO("get all files size")
{
    GIVEN("folder include 6 files")
    {
        FILE *pFile;
        uint16_t result = 0xFFFF;
        uint8_t data[] = "123456789";

        createFolder("test_log1");
        writeFile("test_log1/test1.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test2.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test3.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));

        createFolder("test_log1/test");
        writeFile("test_log1/test/te1st.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test/te2st.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test/te3st.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));

        WHEN("caculate all files size")
        {
            result = getAllFileSize("test_log1");

            THEN("files size is 60 bytes")
            {
                CHECK(result == 60);
            }
            deleteFolder("test_log1");
        }
    }
}

SCENARIO("get specify files size")
{
    GIVEN("folder include 6 files")
    {
        FILE *pFile;
        uint16_t result = 0xFFFF;
        uint8_t data[] = "123456789";

        createFolder("test_log1");
        writeFile("test_log1/test1.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test2.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test3.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));

        createFolder("test_log1/test");
        writeFile("test_log1/test/te1st.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test/te2st.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));
        writeFile("test_log1/test/te3st.txt", FILE_MODE_AU, 0, SEEK_END, data, sizeof(data));

        WHEN("caculate Specify files size")
        {
            result = getSpecifyFileSize("test_log1", "test");

            THEN("files size is 30 bytes")
            {
                CHECK(result == 30);
            }
            deleteFolder("test_log1");
        }
    }
}

SCENARIO("check if the file exists")
{
    GIVEN("create a file")
    {
        uint16_t result = 0xFFFF;
        char existPath[100];

        memset(existPath, '\0', 100);
        createFolder("test_log");
        createFile("test_log/log.txt");

        WHEN("check log.txt, and the file is exists")
        {
            result = checkFileExist(".", "log.txt", existPath);

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }

        WHEN("check log.txt, and the file is not exists")
        {
            deleteFile("test_log/log.txt");
            result = checkFileExist(".", "log.txt", existPath);

            THEN("result is ERR_MISC_FILE_OPEN_ERROR")
            {
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }
        }
        deleteFolder("test_log");
    }
}

SCENARIO("split path into folder and file")
{
    GIVEN("set the path, but path size more than folder + file size")
    {
        std::string resultString;
        uint16_t result = 0xFFFF;
        char path[] = "log1/log2/log3/log4/log.txt";
        char folder[10];
        char file[10];

        WHEN("split path")
        {
            result = splitPathIntoFolderAndFile(path, folder, sizeof(folder), file, sizeof(file));
            THEN("result is ERR_MISC_GET_DATA_FORMAT")
            {
                CHECK(result == ERR_MISC_GET_DATA_FORMAT);
            }
        }
    }
    GIVEN("set the path include folder and file")
    {
        std::string resultString;
        uint16_t result = 0xFFFF;
        char path[] = "log1/log2/log.txt";
        char folder[20];
        char file[20];

        WHEN("split path")
        {
            result = splitPathIntoFolderAndFile(path, folder, sizeof(folder), file, sizeof(file));
            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
            THEN("resultString is 'log1/log2'")
            {
                resultString = (char *)folder;
                CHECK_THAT(resultString, Equals("log1/log2"));
            }
            THEN("resultString is 'log.txt'")
            {
                resultString = (char *)file;
                CHECK_THAT(resultString, Equals("log.txt"));
            }
        }
    }
    GIVEN("set the path only file")
    {
        std::string resultString;
        uint16_t result = 0xFFFF;
        char path[] = "log.txt";
        char folder[20];
        char file[20];

        WHEN("split path")
        {
            result = splitPathIntoFolderAndFile(path, folder, sizeof(folder), file, sizeof(file));
            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
            THEN("resultString is './'")
            {
                resultString = (char *)folder;
                CHECK_THAT(resultString, Equals("./"));
            }
            THEN("resultString is 'log.txt'")
            {
                resultString = (char *)file;
                CHECK_THAT(resultString, Equals("log.txt"));
            }
        }
    }
}

SCENARIO("get file date")
{
    GIVEN("a file name 'test2107125.txt'")
    {
        std::string result;
        uint8_t date[10];

        memset(date, '\0', sizeof(date));

        WHEN("get file date")
        {
            result = getFileDateFromPath("test2107125.txt", "test", date);
            THEN("file date is 210712")
            {
                result = (char *)date;
                CHECK_THAT(result, Equals("210712"));
            }
        }
    }
}

SCENARIO("get file serial number")
{
    GIVEN("a file name 'test2107125.txt' have serial number")
    {
        uint16_t result = 0xFFFF;

        WHEN("get file serial number")
        {
            result = getFileSerialNumberFromPath("test2107125.txt", "210712");

            THEN("file serial number is 5")
            {
                CHECK(result == 5);
            }
        }
    }

    GIVEN("a file name 'test210712.txt' have not serial number")
    {
        uint16_t result = 0xFFFF;

        WHEN("get serial number")
        {
            result = getFileSerialNumberFromPath("test210712.txt", "210712");
            THEN("file serial number is 0")
            {
                CHECK(result == 0);
            }
        }
    }
}

SCENARIO("get current serial number")
{
    GIVEN("a folder include some files")
    {
        uint16_t result = 0xFFFF;
        char file[25];
        createFolder("test1");
        createFolder("test1/test2");
        createFile("test1/test2/test_2107120.txt");
        createFile("test1/test2/test_2107121.txt");
        createFile("test1/test2/test_2107125.txt");
        createFile("test1/test2/test_2107130.txt");
        createFile("test1/test_2107123.txt");
        createFile("test1/test_2107124.txt");
        createFile("test1/test_2107133.txt");

        WHEN("get serial number")
        {
            result = getFileSerialNumberFromFolder("test1", "210712");

            THEN("current serial number is 5")
            {
                CHECK(result == 5);
            }
        }

        deleteFolder("test1");
    }
}

SCENARIO("determine the string is number or not")
{
    GIVEN("a string is '0123456789'")
    {
        uint16_t result = 0xFFFF;
        char string[] = "0123456789";

        WHEN("determine the string")
        {
            result = isAllNumber(string, 10);

            THEN("result is 1")
            {
                CHECK(result == 1);
            }
        }
    }

    GIVEN("a string is '01234A6789'")
    {
        uint16_t result = 0xFFFF;
        char string[] = "01234A6789";

        WHEN("determine the string")
        {
            result = isAllNumber(string, 10);

            THEN("result is 0")
            {
                CHECK(result == 0);
            }
        }
    }
}
