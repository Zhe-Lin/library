#include <string.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "common/custom_errors.h"
#include "file/file_settings.h"
#include "file/file_process.h"
#include "file/file_access.h"
#include "mock/time_mock.hpp"

using Catch::Matchers::Equals;

SCENARIO("initial save file method")
{
    GIVEN("create a file module")
    {
        uint16_t result = 0xFFFF;
        fileObject log;

        initialFileObject(&log);

        WHEN("no save file")
        {
            initialSaveMethod(&log, NO_SAVE_METHOD);
            result = getSaveMethod(&log);

            THEN("result is NO_SAVE_METHOD")
            {
                CHECK(result == NO_SAVE_METHOD);
            }
        }

        WHEN("save file in folder")
        {
            initialSaveMethod(&log, FOLDER_METHOD);
            result = getSaveMethod(&log);

            THEN("result is FOLDER_METHOD")
            {
                CHECK(result == FOLDER_METHOD);
            }
            deleteFolder(FOLDER_NAME);
        }

        WHEN("save file in current folder")
        {
            initialSaveMethod(&log, FILE_METHOD);
            result = getSaveMethod(&log);

            THEN("result is FILE_METHOD")
            {
                CHECK(result == FILE_METHOD);
            }
        }
    }
}

SCENARIO("initial save file path")
{
    GIVEN("use default folder name & file name")
    {
        std::string result;
        char filePath[50];
        fileObject log;

        initialFileObject(&log);

        WHEN("initial no save file")
        {
            initialSaveMethod(&log, NO_SAVE_METHOD);
            initialSavePath(&log);

            THEN("result is no string")
            {
                result = getPath(&log);
                CHECK_THAT(result, Equals(""));
            }
        }

        WHEN("initial save file in folder")
        {
            initialSaveMethod(&log, FOLDER_METHOD);
            initialSavePath(&log);

            THEN("result is 'test_log'")
            {
                result = getPath(&log);
                CHECK_THAT(result, Equals("test_log"));
            }
            deleteFolder(FOLDER_NAME);
        }

        WHEN("initial save file in current folder")
        {
            initialSaveMethod(&log, FILE_METHOD);
            initialSavePath(&log);

            THEN("result is '.'")
            {
                result = getPath(&log);
                CHECK_THAT(result, Equals("."));
            }
        }
    }
}

SCENARIO("get file full name")
{
    GIVEN("use default folder name & file name")
    {
        std::string result;
        fileObject log;
        char fullName[NAME_MAX_LENGTH];

        memset(fullName, '\0', NAME_MAX_LENGTH);
        initialFileObject(&log);

        WHEN("enable date, and get file full name")
        {
            enableFileDate(&log);
            setFileDate(&log, "210714");

            getFileFullName(&log, fullName);

            THEN("result is 'log210714.txt'")
            {
                result = fullName;
                CHECK_THAT(result, Equals("log210714.txt"));
            }
        }

        WHEN("enable serial number, and get file full name")
        {
            enableFileSerialNumber(&log);
            setFileSerialNumber(&log, 9);

            getFileFullName(&log, fullName);

            THEN("result is 'log9.txt'")
            {
                result = fullName;
                CHECK_THAT(result, Equals("log9.txt"));
            }
        }

        WHEN("enable date & serial number, and get file full name")
        {
            enableFileDate(&log);
            setFileDate(&log, "210714");
            enableFileSerialNumber(&log);
            setFileSerialNumber(&log, 9);

            getFileFullName(&log, fullName);

            THEN("result is 'log2107149.txt'")
            {
                result = fullName;
                CHECK_THAT(result, Equals("log2107149.txt"));
            }
        }
    }
}

SCENARIO("get file path")
{
    GIVEN("use default folder name & file name")
    {
        uint16_t resultReturn = 0xFFFF;
        std::string resultString;
        fileObject log;
        char filePath[PATH_MAX_LENGTH];

        memset(filePath, '\0', PATH_MAX_LENGTH);
        initialFileObject(&log);

        WHEN("initial no save file, and get file path")
        {
            initialSaveMethod(&log, NO_SAVE_METHOD);
            resultReturn = getFilePath(&log, filePath);

            THEN("resultReturn is ERR_MISC_NOT_ENABLE_FUNCTION, resultString is no string")
            {
                CHECK(resultReturn == ERR_MISC_NOT_ENABLE_FUNCTION);
                resultString = filePath;
                CHECK_THAT(resultString, Equals(""));
            }
        }

        WHEN("initial save file in current folder, and get file path")
        {
            initialSaveMethod(&log, FILE_METHOD);
            resultReturn = getFilePath(&log, filePath);

            THEN("resultReturn is EXECUTE_OK, resultString is './log.txt'")
            {
                CHECK(resultReturn == EXECUTE_OK);
                resultString = filePath;
                CHECK_THAT(resultString, Equals("./log.txt"));
            }
        }

        WHEN("initial save file in folder, and get file path")
        {
            initialSaveMethod(&log, FOLDER_METHOD);
            resultReturn = getFilePath(&log, filePath);

            THEN("resultReturn is EXECUTE_OK, resultString is 'test_log/log.txt'")
            {
                CHECK(resultReturn == EXECUTE_OK);
                resultString = filePath;
                CHECK_THAT(resultString, Equals("test_log/log.txt"));
            }
        }

        WHEN("initial save file in folder, enable date & serial number, and get file path")
        {
            initialSaveMethod(&log, FOLDER_METHOD);
            enableFileDate(&log);
            setFileDate(&log, "210714");
            enableFileSerialNumber(&log);
            setFileSerialNumber(&log, 9);

            resultReturn = getFilePath(&log, filePath);

            THEN("resultReturn is EXECUTE_OK, resultString is 'test_log/log2107149.txt'")
            {
                CHECK(resultReturn == EXECUTE_OK);
                resultString = filePath;
                CHECK_THAT(resultString, Equals("test_log/log2107149.txt"));
            }
        }
    }
}

SCENARIO("get backup path")
{
    GIVEN("use default folder name & file name")
    {
        uint16_t resultReturn = 0xFFFF;
        std::string resultString;
        fileObject log;
        char backupPath[PATH_MAX_LENGTH];

        memset(backupPath, '\0', PATH_MAX_LENGTH);
        initialFileObject(&log);

        WHEN("initial no save file, and get backup path")
        {
            initialSaveMethod(&log, NO_SAVE_METHOD);
            resultReturn = getBackupPath(&log, backupPath);

            THEN("resultReturn is ERR_MISC_NOT_ENABLE_FUNCTION, resultString is no string")
            {
                CHECK(resultReturn == ERR_MISC_NOT_ENABLE_FUNCTION);
                resultString = backupPath;
                CHECK_THAT(resultString, Equals(""));
            }
        }

        WHEN("initial save file in folder, and get backup path")
        {
            initialSaveMethod(&log, FOLDER_METHOD);
            resultReturn = getBackupPath(&log, backupPath);

            THEN("resultReturn is EXECUTE_OK, resultString is 'test_log/log.backup'")
            {
                CHECK(resultReturn == EXECUTE_OK);
                resultString = backupPath;
                CHECK_THAT(resultString, Equals("test_log/log.backup"));
            }
        }

        WHEN("initial save file in current folder")
        {
            initialSaveMethod(&log, FILE_METHOD);
            resultReturn = getBackupPath(&log, backupPath);

            THEN("resultReturn is EXECUTE_OK, resultString is './log.backup'")
            {
                CHECK(resultReturn == EXECUTE_OK);
                resultString = backupPath;
                CHECK_THAT(resultString, Equals("./log.backup"));
            }
        }
    }
}

SCENARIO("delete file past day")
{
    GIVEN("5 file name different time in folder")
    {
        uint16_t result = 0xFFFF;
        char file[PATH_MAX_LENGTH] = "\0";
        char existPath[PATH_MAX_LENGTH] = "\0";
        fileObject log;

        initialFileObject(&log);
        initialSaveMethod(&log, FOLDER_METHOD);
        initialSavePath(&log);

        sprintf(file, "%s/%s.backup", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s210711.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s210712.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s210713.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s210714.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s210715.txt", getPath(&log), getFileSurname(&log));
        createFile(file);

        ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210716", 9));

        WHEN("disable exist day limit")
        {
            disableExistDay(&log);

            THEN("result is ERR_MISC_NOT_ENABLE_FUNCTION")
            {
                result = deleteFilePastDay(&log);
                CHECK(result == ERR_MISC_NOT_ENABLE_FUNCTION);
            }
        }

        WHEN("enable exist day limit")
        {
            enableExistDay(&log);

            THEN("result is EXECUTE_OK")
            {
                result = deleteFilePastDay(&log);
                CHECK(result == EXECUTE_OK);
            }
        }

        WHEN("enable exist day limit, and delete file 3 days ago from 2021/07/16")
        {
            enableExistDay(&log);
            deleteFilePastDay(&log);

            THEN("test_log/log.backup is still exist")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", ".backup", existPath);
                CHECK(result == EXECUTE_OK);
            }
            THEN("2021/07/11 is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", "210711", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("2021/07/12 is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", "210712", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("2021/07/13 is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", "210713", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("2021/07/14 is EXECUTE_OK")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", "210714", existPath);
                CHECK(result == EXECUTE_OK);
            }

            THEN("2021/07/15 is EXECUTE_OK")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", "210715", existPath);
                CHECK(result == EXECUTE_OK);
            }
            deleteFolder(getFolderName(&log));
        }
    }
}

SCENARIO("delete oldest file")
{
    GIVEN("create 5 files")
    {
        uint16_t result = 0xFFFF;
        char file[PATH_MAX_LENGTH] = "\0";
        char existPath[PATH_MAX_LENGTH] = "\0";
        fileObject log;

        initialFileObject(&log);
        initialSaveMethod(&log, FOLDER_METHOD);
        initialSavePath(&log);

        sprintf(file, "%s/%s.backup", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s2107114.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s2107116.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s2107125.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s2107129.txt", getPath(&log), getFileSurname(&log));
        createFile(file);
        sprintf(file, "%s/%s2107150.txt", getPath(&log), getFileSurname(&log));
        createFile(file);

        WHEN("disable file date")
        {
            disableFileDate(&log);

            THEN("result is ERR_MISC_NOT_ENABLE_FUNCTION")
            {
                result = deleteOldestFile(&log);
                CHECK(result == ERR_MISC_NOT_ENABLE_FUNCTION);
            }
        }

        WHEN("enable file date")
        {
            enableFileDate(&log);

            THEN("result is EXECUTE_OK")
            {
                result = deleteOldestFile(&log);
                CHECK(result == EXECUTE_OK);
            }
        }

        WHEN("enable file date, and delete oldest file")
        {
            enableFileDate(&log);
            deleteOldestFile(&log);

            THEN("test_log/log.backup is still exist")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", ".backup", existPath);
                CHECK(result == EXECUTE_OK);
            }
            THEN("2021/07/11 is ERR_MISC_FILE_OPEN_ERROR")
            {
                memset(existPath, '\0', sizeof(existPath));
                result = checkFileExist("./", "210711", existPath);
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }
            deleteFolder(getFolderName(&log));
        }
    }
}

SCENARIO("check if the file size over the limit")
{
    GIVEN("a file size is 15 bytes")
    {
        uint16_t result = 0xFFFF;
        char filePath[PATH_MAX_LENGTH] = {'\0'};
        char fullName[NAME_MAX_LENGTH] = {'\0'};
        uint8_t data[15];
        fileObject log;

        memset(data, '1', sizeof(data));
        initialFileObject(&log);
        initialSaveMethod(&log, FOLDER_METHOD);
        initialSavePath(&log);
        enableFileDate(&log);
        setFileDate(&log, "210712");

        getFileFullName(&log, fullName);
        sprintf(filePath, "%s/%s", getPath(&log), fullName);

        writeFile(filePath, FILE_MODE_AU, 0x00, SEEK_END, data, sizeof(data));

        WHEN("set size limit is 20, and file size already is 15, but still need 8 bytes space")
        {
            setOneFileMaxSpace(&log, 20);
            result = checkOneFileSize(&log, 8);
            THEN("result is ERR_MISC_NOT_ENOUGH_SPACE")
            {
                CHECK(result == ERR_MISC_NOT_ENOUGH_SPACE);
            }
        }

        WHEN("set size limit is 20, and file size already is 15, but still need 3 bytes space")
        {
            setOneFileMaxSpace(&log, 20);
            result = checkOneFileSize(&log, 3);
            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }

        deleteFolder(getFolderName(&log));
    }
}

SCENARIO("check if all file size over the limit")
{
    GIVEN("a file size is 15 bytes")
    {
        uint16_t result = 0xFFFF;
        char filePath[PATH_MAX_LENGTH] = {'\0'};
        char fullName[NAME_MAX_LENGTH] = {'\0'};
        uint8_t data[15];
        fileObject log;

        memset(data, '1', sizeof(data));
        initialFileObject(&log);
        initialSaveMethod(&log, FOLDER_METHOD);
        initialSavePath(&log);
        enableFileDate(&log);
        setFileDate(&log, "210712");

        getFileFullName(&log, fullName);
        sprintf(filePath, "%s/%s", getPath(&log), fullName);

        writeFile(filePath, FILE_MODE_AU, 0x00, SEEK_END, data, sizeof(data));

        WHEN("set all size limit is 25, one size limit is 15. and file size already is 15")
        {
            setAllFileMaxSpace(&log, 25);
            setOneFileMaxSpace(&log, 15);
            result = checkAllFileSize(&log);
            THEN("result is ERR_MISC_NOT_ENOUGH_SPACE")
            {
                CHECK(result == ERR_MISC_NOT_ENOUGH_SPACE);
            }
        }

        WHEN("set all size limit is 40, one size limit is 15. and file size already is 15")
        {
            setAllFileMaxSpace(&log, 40);
            setOneFileMaxSpace(&log, 20);
            result = checkAllFileSize(&log);
            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }

        deleteFolder(getFolderName(&log));
    }
}

SCENARIO("write backup data")
{
    fileObject log;
    initialFileObject(&log);

    GIVEN("write data is 'test:123456'")
    {
        std::string result;
        char data[] = "123456";

        WHEN("write to backup data")
        {
            writeBackupToTempData(&log, "test:%s", data);

            THEN("backup data is 'test:123456'")
            {
                result = getBackupTempData(&log);
                CHECK_THAT(result, Equals("test:123456"));
            }
        }
    }
}

SCENARIO("backup file data")
{
    fileObject log;
    initialFileObject(&log);
    enableBackup(&log);
    initialSaveMethod(&log, FOLDER_METHOD);
    initialSavePath(&log);

    GIVEN("set date is '210803', serial number is 5")
    {
        uint16_t result = 0xFFFF;
        setFileDate(&log, "210803");
        setFileSerialNumber(&log, 5);
        writeBackupToTempData(&log, "%s%s%s%d\r\n", FILE_KEY_NAME_DATE, getFileDate(&log), FILE_KEY_NAME_SN, getFileSerialNumber(&log));

        WHEN("disable backup")
        {
            disableBackup(&log);
            result = updateBackupFileFromTempData(&log);

            THEN("result is ERR_MISC_NOT_ENABLE_FUNCTION")
            {
                CHECK(result == ERR_MISC_NOT_ENABLE_FUNCTION);
            }
        }

        WHEN("enable backup")
        {
            enableBackup(&log);
            result = updateBackupFileFromTempData(&log);

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }

        deleteFolder(getFolderName(&log));
    }
}

SCENARIO("restore data from backup file")
{
    fileObject log;
    initialFileObject(&log);
    enableBackup(&log);
    initialSaveMethod(&log, FOLDER_METHOD);
    initialSavePath(&log);

    GIVEN("set date is '210803', serial number is 5")
    {
        uint16_t result = 0xFFFF;
        std::string resultString;
        char data[10] = {'\0'};

        setFileDate(&log, "210803");
        setFileSerialNumber(&log, 5);
        writeBackupToTempData(&log, "%s%s%s%d\r\n", FILE_KEY_NAME_DATE, getFileDate(&log), FILE_KEY_NAME_SN, getFileSerialNumber(&log));
        updateBackupFileFromTempData(&log);

        WHEN("disable backup")
        {
            disableBackup(&log);
            result = restoreDataFromBackup(&log, FILE_KEY_NAME_DATE, data);

            THEN("result is ERR_MISC_NOT_ENABLE_FUNCTION")
            {
                CHECK(result == ERR_MISC_NOT_ENABLE_FUNCTION);
            }
        }

        WHEN("enable backup")
        {
            enableBackup(&log);
            result = restoreDataFromBackup(&log, FILE_KEY_NAME_DATE, data);

            THEN("result is EXECUTE_OK")
            {
                CHECK(result == EXECUTE_OK);
            }
        }

        WHEN("have backup file, and restore data")
        {
            memset(data, '\0', sizeof(data));

            THEN("date is '210803'")
            {
                result = restoreDataFromBackup(&log, FILE_KEY_NAME_DATE, data);
                resultString = data;
                CHECK(result == EXECUTE_OK);
                CHECK_THAT(resultString, Equals("210803"));
            }

            // memset(data, '\0', sizeof(data));

            THEN("serial number is '5'")
            {
                result = restoreDataFromBackup(&log, FILE_KEY_NAME_SN, data);
                resultString = data;
                CHECK(result == EXECUTE_OK);
                CHECK_THAT(resultString, Equals("5"));
            }
            deleteFolder(getFolderName(&log));
        }

        WHEN("not backup file, and restore data")
        {
            deleteSpecifyFile(getFolderName(&log), getBackupExtension(&log));
            result = restoreDataFromBackup(&log, FILE_KEY_NAME_DATE, data);

            THEN("result is ERR_MISC_FILE_OPEN_ERROR")
            {
                CHECK(result == ERR_MISC_FILE_OPEN_ERROR);
            }
            deleteFolder(getFolderName(&log));
        }
    }
}

SCENARIO("initial file date")
{
    GIVEN("use default folder name & file name")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);
        ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210804", 9));

        WHEN("initial date")
        {
            initialFileDate(&log);

            THEN("result is '210804'")
            {
                result = getFileDate(&log);
                CHECK_THAT(result, Equals("210804"));
            }
        }
    }
}

SCENARIO("initial serial number")
{
    uint32_t result = 0xFFFF;
    fileObject log;
    char path[PATH_MAX_LENGTH] = {'\0'};

    initialFileObject(&log);
    initialSaveMethod(&log, FOLDER_METHOD);
    initialSavePath(&log);

    sprintf(path, "%s/%s_2107120.txt", getPath(&log), getFileSurname(&log));
    createFile(path);

    sprintf(path, "%s/%s_2107121.txt", getPath(&log), getFileSurname(&log));
    createFile(path);

    sprintf(path, "%s/%s_2107125.txt", getPath(&log), getFileSurname(&log));
    createFile(path);

    sprintf(path, "%s/%s_2107126.txt", getPath(&log), getFileSurname(&log));
    createFile(path);

    ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210712", 9));

    GIVEN("not backup file")
    {
        WHEN("initial serial number")
        {
            initialFileSerialNumber(&log);

            THEN("result is 6")
            {
                result = getFileSerialNumber(&log);
                CHECK(result == 6);
            }
        }
    }

    GIVEN("set date is '210712', the same as mock, serial number is 5,and backup data")
    {
        enableBackup(&log);
        setFileDate(&log, "210712");
        setFileSerialNumber(&log, 5);
        writeBackupToTempData(&log, "%s%s%s%d\r\n", FILE_KEY_NAME_DATE, getFileDate(&log), FILE_KEY_NAME_SN, getFileSerialNumber(&log));
        updateBackupFileFromTempData(&log);

        WHEN("initial serial number")
        {
            initialFileSerialNumber(&log);

            THEN("result is 5")
            {
                result = getFileSerialNumber(&log);
                CHECK(result == 5);
            }
        }
    }

    GIVEN("set date is '210714', newer than mock, serial number is 4,and backup data")
    {
        enableBackup(&log);
        setFileDate(&log, "210714");
        setFileSerialNumber(&log, 4);
        writeBackupToTempData(&log, "%s%s%s%d\r\n", FILE_KEY_NAME_DATE, getFileDate(&log), FILE_KEY_NAME_SN, getFileSerialNumber(&log));
        updateBackupFileFromTempData(&log);

        WHEN("initial serial number")
        {
            initialFileSerialNumber(&log);

            THEN("result is 0")
            {
                result = getFileSerialNumber(&log);
                CHECK(result == 0);
            }
        }

        deleteFolder(getFolderName(&log));
    }
}

SCENARIO("roll serial number")
{
    GIVEN("set serial number is 5")
    {
        uint16_t result = 0xFFFF;
        fileObject log;

        initialFileObject(&log);
        setFileSerialNumber(&log, 5);

        WHEN("disable serial number, and roll serial number")
        {
            disableFileSerialNumber(&log);
            result = rollSerialNumber(&log);

            THEN("return is ERR_MISC_NOT_ENABLE_FUNCTION, and serial number still is 5")
            {
                CHECK(result == ERR_MISC_NOT_ENABLE_FUNCTION);
                result = getFileSerialNumber(&log);
                CHECK(result == 5);
            }
        }

        WHEN("enable serial number, and roll serial number")
        {
            enableFileSerialNumber(&log);
            result = rollSerialNumber(&log);

            THEN("return is EXECUTE_OK, and serial number still is 6")
            {
                CHECK(result == EXECUTE_OK);
                result = getFileSerialNumber(&log);
                CHECK(result == 6);
            }
        }
    }

    GIVEN("set serial number is 9")
    {
        uint16_t result = 0xFFFF;
        fileObject log;

        initialFileObject(&log);
        enableFileSerialNumber(&log);
        setFileSerialNumber(&log, 9);

        WHEN("enable serial number, and roll serial number")
        {
            rollSerialNumber(&log);

            THEN("return is EXECUTE_OK, and serial number still is 0")
            {
                result = getFileSerialNumber(&log);
                CHECK(result == 0);
            }
        }
    }
}

SCENARIO("check whether the day has changed")
{
    GIVEN("file date is '210805', current date is 210809")
    {
        std::string resultString;
        uint16_t resultReturn = 0xFFFF;
        fileObject log;
        initialFileObject(&log);
        setFileDate(&log, "210805");

        ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210809", 9));

        WHEN("disable date, and check date")
        {
            disableFileDate(&log);
            resultReturn = checkDate(&log);
            resultString = getFileDate(&log);

            THEN("return is ERR_MISC_NOT_ENABLE_FUNCTION, and file date is '210805'")
            {
                CHECK(resultReturn == ERR_MISC_NOT_ENABLE_FUNCTION);
                CHECK_THAT(resultString, Equals("210805"));
            }
        }

        WHEN("enable date, and check date")
        {
            enableFileDate(&log);
            resultReturn = checkDate(&log);
            resultString = getFileDate(&log);

            THEN("return is EXECUTE_OK, and file date is '210809'")
            {
                CHECK(resultReturn == EXECUTE_OK);
                CHECK_THAT(resultString, Equals("210809"));
            }
        }
    }

    GIVEN("file date is '210805', current date is 210805")
    {
        std::string resultString;
        uint16_t resultReturn = 0xFFFF;
        fileObject log;
        initialFileObject(&log);
        setFileDate(&log, "210805");

        ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210805", 9));

        WHEN("enable date, and check date")
        {
            enableFileDate(&log);
            checkDate(&log);
            resultString = getFileDate(&log);

            THEN("return is EXECUTE_OK, and file date is '210805'")
            {
                CHECK_THAT(resultString, Equals("210805"));
            }
        }
    }

    GIVEN("file date is '210805', current date is 210801")
    {
        std::string resultString;
        uint16_t resultReturn = 0xFFFF;
        fileObject log;
        initialFileObject(&log);
        setFileDate(&log, "210805");

        ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210801", 9));

        WHEN("enable date, and check date")
        {
            enableFileDate(&log);
            checkDate(&log);
            resultString = getFileDate(&log);

            THEN("return is EXECUTE_OK, and file date is '210805'")
            {
                CHECK_THAT(resultString, Equals("210805"));
            }
        }
    }
}

SCENARIO("create and write log")
{
    std::string resultString;
    uint16_t resultReturn = 0xFFFF;
    fileObject log = {'\0'};
    char data[200] = {'\0'};

    ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210810", 9));

    createLog(&log);

    GIVEN("an array to save path")
    {
        char existPath[PATH_MAX_LENGTH] = {'\0'};

        WHEN("whrite log")
        {
            memset(data, '\0', sizeof(data));
            sprintf(data, "123456");
            writeBackupToTempData(&log, "%s%s%s%d\r\n", FILE_KEY_NAME_DATE, getFileDate(&log), FILE_KEY_NAME_SN, getFileSerialNumber(&log));
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, sizeof(data));

            THEN("log file exist")
            {
                resultReturn = checkFileExist(getFolderName(&log), getFileExtension(&log), existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("backup file exist")
            {
                resultReturn = checkFileExist(getFolderName(&log), getBackupExtension(&log), existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }
        }
    }

    GIVEN("date is '210803', and serial number is 5")
    {
        setFileDate(&log, "210813");
        setFileSerialNumber(&log, 5);

        WHEN("whrite log")
        {
            memset(data, '\0', sizeof(data));
            sprintf(data, "123456");
            writeBackupToTempData(&log, "%s%s%s%d\r\n", FILE_KEY_NAME_DATE, getFileDate(&log), FILE_KEY_NAME_SN, getFileSerialNumber(&log));
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, sizeof(data));

            THEN("backup date is '210813'")
            {
                memset(data, '\0', sizeof(data));
                restoreDataFromBackup(&log, FILE_KEY_NAME_DATE, data);
                resultString = data;
                CHECK_THAT(resultString, Equals("210813"));
            }

            THEN("serial number is '5'")
            {
                memset(data, '\0', sizeof(data));
                restoreDataFromBackup(&log, FILE_KEY_NAME_SN, data);
                resultString = data;
                CHECK_THAT(resultString, Equals("5"));
            }
        }
    }

    GIVEN("create 4 file, and set exist day is 3")
    {
        char existPath[PATH_MAX_LENGTH] = {'\0'};

        createFile("Log/IPLOG.backup");
        createFile("Log/IPLOG210803.dat");
        createFile("Log/IPLOG210805.dat");
        createFile("Log/IPLOG210807.dat");
        createFile("Log/IPLOG210809.dat");

        setFileExistDay(&log, 3);

        WHEN("delete file past 3 day")
        {
            deleteFilePastDay(&log);

            THEN("IPLOG210803.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "210803", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("IPLOG210805.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "210805", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("IPLOG210807.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "210807", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("IPLOG210809.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "210809", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG.backup is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "IPLOG.backup", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }
        }
    }

    GIVEN("file all have 100 size, and set one file space is 125, set SN is 6")
    {
        char existPath[PATH_MAX_LENGTH] = {'\0'};

        memset(data, '\0', sizeof(data));
        memset(data, '1', 100);
        createFile("Log/IPLOG2108106.dat");
        writeFile("Log/IPLOG2108106.dat", FILE_MODE_AU, 0, SEEK_END, (uint8_t *)data, strlen(data));

        setOneFileMaxSpace(&log, 125);
        setFileSerialNumber(&log, 6);

        WHEN("haven't reach one file max space")
        {
            memset(data, '\0', sizeof(data));
            memset(data, '1', 20);
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));

            THEN("IPLOG2108106.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108106", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108107.dat isn't exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108107", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("all file size is 120")
            {
                resultReturn = getAllFileSize(getFolderName(&log));
                CHECK(resultReturn == 120);
            }
        }

        WHEN("reach one file max space")
        {
            memset(data, '\0', sizeof(data));
            memset(data, '1', 120);
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));

            THEN("IPLOG2108106.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108106", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108107.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108107", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("all file size is 220")
            {
                resultReturn = getAllFileSize(getFolderName(&log));
                CHECK(resultReturn == 220);
            }
        }

        WHEN("reach one file max space,and next SN is exist")
        {
            memset(data, '\0', sizeof(data));
            memset(data, '1', 120);

            createFile("Log/IPLOG2108107.dat");
            writeFile("Log/IPLOG2108107.dat", FILE_MODE_AU, 0, SEEK_END, (uint8_t *)data, strlen(data));

            memset(data, '\0', sizeof(data));
            memset(data, '1', 40);
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));

            THEN("IPLOG2108106.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108106", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108107.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108107", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108107.dat size is 40")
            {
                resultReturn = getSpecifyFileSize(getFolderName(&log), "2108107");
                CHECK(resultReturn == 40);
            }

            THEN("all file size is 140")
            {
                resultReturn = getAllFileSize(getFolderName(&log));
                CHECK(resultReturn == 140);
            }
        }
    }

    GIVEN("files all have 200 size, and set all file space is 250, set SN is 6")
    {
        char existPath[PATH_MAX_LENGTH] = {'\0'};

        memset(data, '\0', sizeof(data));
        memset(data, '1', 100);
        createFile("Log/IPLOG2108030.dat");
        writeFile("Log/IPLOG2108030.dat", FILE_MODE_AU, 0, SEEK_END, (uint8_t *)data, strlen(data));
        createFile("Log/IPLOG2108050.dat");
        writeFile("Log/IPLOG2108050.dat", FILE_MODE_AU, 0, SEEK_END, (uint8_t *)data, strlen(data));

        setAllFileMaxSpace(&log, 250);
        setOneFileMaxSpace(&log, 40);
        setFileSerialNumber(&log, 6);

        WHEN("haven't reach all file max space")
        {
            memset(data, '\0', sizeof(data));
            memset(data, '1', 40);
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));

            THEN("IPLOG2108030.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108030", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108050.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108050", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108106.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "IPLOG2108106.dat", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("all file size is 240")
            {
                resultReturn = getAllFileSize(getFolderName(&log));
                CHECK(resultReturn == 240);
            }
        }

        WHEN("reach all file max space once")
        {
            memset(data, '\0', sizeof(data));
            memset(data, '1', 40);
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));

            THEN("IPLOG2108030.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108030", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("IPLOG2108050.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108050", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108106.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "IPLOG2108106.dat", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("all file size is 180")
            {
                resultReturn = getAllFileSize(getFolderName(&log));
                CHECK(resultReturn == 180);
            }
        }

        WHEN("reach all file max space twice")
        {
            memset(data, '\0', sizeof(data));
            memset(data, '1', 40);
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));
            writeProcess(&log, (char *)FILE_MODE_AU, SEEK_END, (uint8_t *)data, strlen(data));

            THEN("IPLOG2108030.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108030", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("IPLOG2108050.dat not exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "2108050", existPath);
                CHECK(resultReturn == ERR_MISC_FILE_OPEN_ERROR);
            }

            THEN("IPLOG2108106.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "IPLOG2108106.dat", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("IPLOG2108107.dat is exists")
            {
                resultReturn = checkFileExist(getFolderName(&log), "IPLOG2108107.dat", existPath);
                CHECK(resultReturn == EXECUTE_OK);
            }

            THEN("all file size is 160")
            {
                resultReturn = getAllFileSize(getFolderName(&log));
                CHECK(resultReturn == 160);
            }
        }
    }

    deleteFolder(getFolderName(&log));
}

SCENARIO("filter path")
{
    GIVEN("a path: 'Log/IPLOG2108106.dat'")
    {
        std::string result;
        uint8_t path[] = "Log/IPLOG2108106.dat";

        WHEN("filter path")
        {
            result = filterPath((char *)path);
            THEN("result is 'Log/IPLOG2108106.dat'")
            {
                CHECK_THAT(result, Equals("Log/IPLOG2108106.dat"));
            }
        }
    }
}

SCENARIO("[bug] filter path")
{
    GIVEN("two path: 'Log/IPLOG2108106.dat' & 'Log/IPLOG2108107.dat'")
    {
        std::string result;
        char path1[] = "Log/IPLOG2108106.dat";
        char path2[] = "Log/IPLOG2108107.dat";
        char path[100];

        WHEN("filter two path")
        {

            sprintf(path, "path1:%s ; path2:%s", filterPath(path1), filterPath(path2));

            THEN("result should is 'path1:Log/IPLOG2108106.dat ; path2:Log/IPLOG2108107.dat', but is 'path1:Log/IPLOG2108106.dat ; path2:Log/IPLOG2108106.dat'")
            {
                result = path;
                CHECK_THAT(result, Equals("path1:Log/IPLOG2108106.dat ; path2:Log/IPLOG2108106.dat"));
            }
        }
    }
}
