#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message2/message.h"
#include "message2/message_component.h"

using Catch::Matchers::Equals;

SCENARIO("static command")
{
  void *otaMessage = message_moduleNew("ota");

  GIVEN("register static command")
  {
    message_bindingStaticCommand(otaMessage, "Cmd", 2);

    WHEN("receive data")
    {
      message_receiveHandler(otaMessage, "\x31\x32", 2);

      THEN("Cmd component is done")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        messageState result = specifyComponent->state;
        CHECK(result == stateDone);
      }

      THEN("Cmd is '12'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        std::string result = (char *)specifyComponent->receiveData;

        CHECK_THAT(result, Equals("12"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("variable command")
{
  void *otaMessage = message_moduleNew("ota");

  GIVEN("register variable command")
  {
    message_bindingVariableCommand(otaMessage, "Cmd", 2);

    WHEN("receive variable command length is 1000")
    {
      message_receiveHandler(otaMessage, "\x03\xE8", 2);

      THEN("Cmd component bytes number is 1000")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        uint32_t result = specifyComponent->receiveBytesNum;
        CHECK(result == 1000);
      }
    }
  }

  GIVEN("register variable command")
  {
    message_bindingVariableCommand(otaMessage, "Cmd", 1);

    WHEN("receive data")
    {
      message_receiveHandler(otaMessage, "\x03\x31\x32\x33", 4);

      THEN("Cmd component is done")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        messageState result = specifyComponent->state;
        CHECK(result == stateDone);
      }

      THEN("Cmd is '123'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        std::string result = (char *)specifyComponent->receiveData;

        CHECK_THAT(result, Equals("123"));
      }
    }
  }

  message_moduleFree(otaMessage);
}
