#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message2/message.h"
#include "message2/message_component.h"

using Catch::Matchers::Equals;

SCENARIO("assign component")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("assign static space component")
  {
    messageComponent *component = message_assignStaticSpace(otaMessage, "static", sizeof(messageComponent), 3);

    WHEN("component name is 'static'")
    {
      std::string result;
      result = (char *)component->spaceName;

      THEN("result is 'static'")
      {
        CHECK_THAT(result, Equals("static"));
      }
    }

    WHEN("space length is 3")
    {
      uint32_t result = 0;
      result = component->receiveBytesNum;

      THEN("result is 3")
      {
        CHECK(result == 3);
      }
    }
  }

  GIVEN("assign variable space component")
  {
    messageComponent *component = message_assignVariableSpace(otaMessage, "variable", sizeof(messageComponent));

    WHEN("component name is 'variable'")
    {
      std::string result;
      result = (char *)component->spaceName;

      THEN("result is 'variable'")
      {
        CHECK_THAT(result, Equals("variable"));
      }
    }

    WHEN("space length is 0")
    {
      uint32_t result = 0xFF;
      result = component->receiveBytesNum;

      THEN("result is 0")
      {
        CHECK(result == 0);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("save data")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("receive 10 pieces of data")
  {
    messageComponent *component = message_assignStaticSpace(otaMessage, "static", sizeof(messageComponent), 5);
    uint8_t data[] = "1234567890";

    WHEN("save 10 pieces of data")
    {
      uint32_t result = 0;

      result = message_componentReceiveSaveData(otaMessage, component, data, 10);

      THEN("result is 5")
      {
        CHECK(result == 5);
      }
    }

    WHEN("save 3 pieces of data")
    {
      uint32_t result = 0;

      result = message_componentReceiveSaveData(otaMessage, component, data, 3);

      THEN("result is 3")
      {
        CHECK(result == 3);
      }
    }

    WHEN("continuous 2 times to save 3 pieces of data")
    {
      uint32_t result = 0;

      result = message_componentReceiveSaveData(otaMessage, component, data, 3);

      THEN("first receive: result is 3")
      {
        CHECK(result == 3);
      }

      result = message_componentReceiveSaveData(otaMessage, component, data, 3);

      THEN("second receive: result is 2")
      {
        CHECK(result == 2);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("get specify component")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("assign 3 static space components")
  {
    messageComponent *component1 = message_assignStaticSpace(otaMessage, "static1", sizeof(messageComponent), 4);
    messageComponent *component2 = message_assignStaticSpace(otaMessage, "static2", sizeof(messageComponent), 4);
    messageComponent *component3 = message_assignStaticSpace(otaMessage, "static3", sizeof(messageComponent), 4);

    WHEN("get component that name is 'static2'")
    {
      std::string result;
      messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "static2");
      result = (char *)specifyComponent->spaceName;

      THEN("result is 'static2'")
      {
        CHECK_THAT(result, Equals("static2"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("get specify process")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("assign 3 process")
  {
    message_bindingVariableData(otaMessage, "Cmd", 1);
    message_assignProcess(otaMessage, "test1", "Cmd", NULL);
    message_assignProcess(otaMessage, "test2", "Cmd", NULL);
    message_assignProcess(otaMessage, "test3", "Cmd", NULL);

    WHEN("get specify process that name is 'test2'")
    {
      messageProcess *process = (messageProcess *)message_processGetSpecify(otaMessage, "test2");

      THEN("result is 'test2'")
      {
        std::string result;
        result = (char *)process->name;
        CHECK_THAT(result, Equals("test2"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("clear state")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("register components & receive data")
  {
    message_bindingStaticCommand(otaMessage, "Cmd", 2);
    message_bindingStaticData(otaMessage, "Data", 2);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    message_receiveHandler(otaMessage, "\x31\x32\x33\x34\x16", 5);

    WHEN("clear all state")
    {
      message_componentClearAllStatus(otaMessage);

      THEN("Cmd state is undo")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateUndo);
      }

      THEN("Data state is undo")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateUndo);
      }

      THEN("CRC state is undo")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateUndo);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("component forward or return to starting point")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("register components & receive data")
  {
    message_bindingStaticCommand(otaMessage, "Cmd", 2);
    message_bindingStaticData(otaMessage, "Data", 2);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    messageComponent *current = otaMessage->components.specify;

    WHEN("component pointer forward one step")
    {
      current = message_componentGetNext(current);

      THEN("pointer is 'Data'")
      {
        std::string result;
        result = (char *)current->spaceName;

        CHECK_THAT(result, Equals("Data"));
      }
    }

    WHEN("component pointer forward two step")
    {
      current = message_componentGetNext(current);
      current = message_componentGetNext(current);


      THEN("pointer is 'CRC'")
      {
        std::string result;
        result = (char *)current->spaceName;

        CHECK_THAT(result, Equals("CRC"));
      }
    }

    WHEN("component pointer forward two step and return to starting point")
    {
      current = message_componentGetNext(current);
      current = message_componentGetNext(current);
      current = message_componentGetFirst(otaMessage);

      THEN("pointer is 'Cmd'")
      {
        std::string result;
        result = (char *)current->spaceName;

        CHECK_THAT(result, Equals("Cmd"));
      }
    }

    WHEN("component pointer last")
    {
      current = message_componentGetLast(otaMessage);

      THEN("pointer is 'CRC'")
      {
        std::string result;
        result = (char *)current->spaceName;

        CHECK_THAT(result, Equals("CRC"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("save send data")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("register static components, and space is enough")
  {
    message_bindingStaticData(otaMessage, "Data", 6);

    WHEN("save send data")
    {
      message_sendSaveData(otaMessage, "Data", "test", 4);

      THEN("Data component's sendData is 'test'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        std::string result;
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("test"));
      }
    }
  }

  GIVEN("register static components, but space is shortage")
  {
    message_bindingStaticData(otaMessage, "Data", 2);

    WHEN("save send data")
    {
      message_sendSaveData(otaMessage, "Data", "test", 4);

      THEN("Data component's sendData is 'te'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        std::string result;
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("te"));
      }
    }
  }

  GIVEN("register variable components")
  {
    message_bindingVariableData(otaMessage, "Data", 2);

    WHEN("save send data")
    {
      message_sendSaveData(otaMessage, "Data", "test", 4);

      THEN("Data component's sendData is 'test'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        std::string result;
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("test"));
      }

      THEN("DataLen component's sendData is '0x00 0x04'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "DataLen");
        uint8_t result[] = { 0x00, 0x04 };

        CHECK(memcmp(result, specifyComponent->sendData, 2) == 0);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("save response data")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("register static components, and space is enough")
  {
    message_bindingStaticData(otaMessage, "Data", 6);
    message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");

    WHEN("save response data")
    {
      message_sendSaveResponse(otaMessage, true);

      THEN("Data component's sendData is 'TRUE'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        std::string result;
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("TRUE"));
      }
    }
  }

  GIVEN("register static components, but space is shortage")
  {
    message_bindingStaticData(otaMessage, "Data", 2);
    message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");

    WHEN("save response data")
    {
      message_sendSaveResponse(otaMessage, true);

      THEN("Data component's sendData is 'TR'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        std::string result;
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("TR"));
      }
    }
  }

  GIVEN("register variable components")
  {
    message_bindingVariableData(otaMessage, "Data", 2);
    message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");

    WHEN("save response data")
    {
      message_sendSaveResponse(otaMessage, true);

      THEN("Data component's sendData is 'TRUE'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        std::string result;
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("TRUE"));
      }

      THEN("DataLen component's sendData is '0x00 0x04'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "DataLen");
        uint8_t result[] = { 0x00, 0x04 };

        CHECK(memcmp(result, specifyComponent->sendData, 2) == 0);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("save response command")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("register static components, and space is enough")
  {
    message_bindingStaticCommand(otaMessage, "Cmd", 6);
    message_bindingStaticCommand(otaMessage, "Data", 6);
    message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");

    WHEN("receive data")
    {
      message_receiveHandler(otaMessage, "123456abcdef", 12);

      THEN("Cmd component's sendData is '123456'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("123456"));
      }
    }
  }

  GIVEN("register variable components")
  {
    message_bindingVariableCommand(otaMessage, "Cmd", 2);
    message_bindingVariableData(otaMessage, "Data", 2);
    message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");

    WHEN("receive data")
    {
      message_receiveHandler(otaMessage, "\x00\x06\x31\x32\x33\x34\x35\x36\x00\x06\x61\x62\x63\x64\x65\x66", 16);

      THEN("Cmd component's sendData is '\x31\x32\x33\x34\x35\x36'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        uint8_t result[] = { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36 };

        CHECK(memcmp(result, specifyComponent->sendData, 6) == 0);
      }

      THEN("CmdLen component's sendData is '\x00\x06'")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CmdLen");
        uint8_t result[] = { 0x00, 0x06 };

        CHECK(memcmp(result, specifyComponent->sendData, 2) == 0);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("number & bytes convert")
{
  GIVEN("conver number to bytes")
  {
    WHEN("give number is '625' & specify bytes number is '2'")
    {
      unsigned char *convertBuffer = message_componentGetNumberConvertBytesBigEndian(625, 2);

      THEN("convert result is '0x02 0x71'")
      {
        unsigned char result[] = { 0x02, 0x71 };

        CHECK(memcmp(result, convertBuffer, 2) == 0);
      }

      free(convertBuffer);
    }

    WHEN("give number is '789' & not specify bytes number")
    {
      unsigned char *convertBuffer = message_componentGetNumberConvertBytesBigEndian(789, 0);

      THEN("convert result is '0x03 0x15'")
      {
        unsigned char result[] = { 0x03, 0x15 };

        CHECK(memcmp(result, convertBuffer, 2) == 0);
      }

      free(convertBuffer);
    }
  }

  GIVEN("conver bytes to number")
  {
    WHEN("give bytes is '0x03 0x54'")
    {
      unsigned int number = message_componentGetBytesConvertNumberBigEndian("\x03\x54", 2);

      THEN("convert result is '852'")
      {
        unsigned int result = number;

        CHECK(result == 852);
      }
    }
  }
}