#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message2/message.h"
#include "message2/message_component.h"

using Catch::Matchers::Equals;

SCENARIO("tail check")
{
  void *otaMessage = message_moduleNew("ota");

  GIVEN("register tail")
  {
    message_bindingTail(otaMessage, "Tail", "\x5A\xA5", 2);

    WHEN("head code be setted up to sendData")
    {
      messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Tail");

      THEN("sendData is '\x5A\xA5'")
      {
        std::string result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("\x5A\xA5"));
      }
    }

    WHEN("receive 1 pieces of data")
    {
      message_receiveHandler(otaMessage, "\x5A", 1);

      THEN("tail state is doing")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Tail");
        messageState result = specifyComponent->state;
        CHECK(result == stateDoing);
      }
    }

    WHEN("receive correct data")
    {
      message_receiveHandler(otaMessage, "\x5A\xA5", 2);

      THEN("tail state is done")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Tail");
        messageState result = specifyComponent->state;
        CHECK(result == stateDone);
      }
    }

    WHEN("receive incorrect data")
    {
      message_receiveHandler(otaMessage, "\x0A\xA0", 2);

      THEN("tail state is undo")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Tail");
        messageState result = specifyComponent->state;
        CHECK(result == stateUndo);
      }
    }
  }

  message_moduleFree(otaMessage);
}
