#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message2/message_component.h"
#include "message2/message.h"

using Catch::Matchers::Equals;

#ifdef __cplusplus
extern "C" {
#endif
  void message_receiveIdleHandler(void *specifyModule);
  void message_sendIdleHandler(void *specifyModule);
#ifdef __cplusplus
}
#endif
bool message_mapTest1(void *specifyModule, void *specifyComponent) {
  printf("========== map test1 ==========\r\n");
  return true;
}

bool message_mapTest2(void *specifyModule, void *specifyComponent) {
  printf("========== map test2 ==========\r\n");
  return true;
}

bool message_mapTest3(void *specifyModule, void *specifyComponent) {
  printf("========== map test3 ==========\r\n");
  return true;
}

int message_callBack(void *port, const char *data, int dataLength) {
  // for (uint32_t i = 0; i < dataLength; i++)
  // {
  //   printf("%x ", data[i]);
  // }
  return 0;
}

SCENARIO("receive data")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("receive 10 pieces of data")
  {
    messageComponent *component1 = message_assignStaticSpace(otaMessage, "static1", sizeof(messageComponent), 5);
    messageComponent *component2 = message_assignStaticSpace(otaMessage, "static2", sizeof(messageComponent), 5);
    messageComponent *component3 = message_assignStaticSpace(otaMessage, "static3", sizeof(messageComponent), 5);

    char data[] = "12345678901234567890";
    message_receiveHandler(otaMessage, data, 6);

    WHEN("receive 6 pieces of data")
    {
      uint32_t result = 0;

      THEN("component1 state is done")
      {
        result = message_componentGetState(component1);
        CHECK(result == stateDone);
      }

      THEN("component2 state is doing")
      {
        result = message_componentGetState(component2);
        CHECK(result == stateDoing);
      }

      THEN("component3 state is undo")
      {
        result = message_componentGetState(component3);
        CHECK(result == stateUndo);
      }
    }

    message_receiveHandler(otaMessage, data, 6);

    WHEN("receive 6 pieces of data again")
    {
      uint32_t result = 0;

      THEN("component1 state is done")
      {
        result = message_componentGetState(component1);
        CHECK(result == stateDone);
      }

      THEN("component2 state is done")
      {
        result = message_componentGetState(component2);
        CHECK(result == stateDone);
      }

      THEN("component3 state is doing")
      {
        result = message_componentGetState(component3);
        CHECK(result == stateDoing);
      }
    }
  }

  GIVEN("assign 3 map process")
  {
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_registerProcess(otaMessage, "test1", "Cmd", NULL);
    message_registerProcess(otaMessage, "test2", "Cmd", NULL);
    message_registerProcess(otaMessage, "test3", "Cmd", NULL);
    messageComponent *component = message_componentGetSpecify(otaMessage, "Cmd");

    WHEN("receive command is 'test2'")
    {
      char data[] = "\x05test2";
      message_receiveHandler(otaMessage, data, 6);

      THEN("according command to specify process")
      {
        bool result;
        result = component->receiveIdleProcess(otaMessage, component);
        CHECK(result == true);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("receive idle process")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");

  GIVEN("register components")
  {
    message_bindingHead(otaMessage, "Head", "\x5A\xA5", 2);
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_bindingVariableData(otaMessage, "Data", 1);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    message_registerProcess(otaMessage, "test1", "Data", NULL);
    message_registerProcess(otaMessage, "test2", "Data", NULL);
    message_registerProcess(otaMessage, "test3", "Data", NULL);

    WHEN("receive data & idle process")
    {
      message_receiveHandler(otaMessage, "\x5A\xA5\x05\x74\x65\x73\x74\x32\x02\x33\x34\x83", 12);
      message_receiveIdleHandler(otaMessage);

      THEN("get data is '34'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetReceiveData(specifyComponent);
        CHECK_THAT(result, Equals("34"));
      }
    }

    WHEN("receive two pieces of data & idle process")
    {
      message_receiveHandler(otaMessage, "\x5A\xA5\x05\x74\x65\x73\x74\x32\x02\x33\x34\x83", 12);
      message_receiveIdleHandler(otaMessage);

      message_receiveHandler(otaMessage, "\x5A\xA5\x05\x74\x65\x73\x74\x33\x02\x34\x35\xC2", 12);
      message_receiveIdleHandler(otaMessage);

      THEN("get data is '45'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetReceiveData(specifyComponent);
        CHECK_THAT(result, Equals("45"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("send idle process")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");
  message_sendSetCallBack(otaMessage, NULL, message_callBack);

  GIVEN("register components")
  {
    message_bindingHead(otaMessage, "Head", "\x5A\xA5", 2);
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_bindingVariableData(otaMessage, "Data", 1);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    WHEN("idle process sendData:'0x5A 0xA5 0x03 0x31 0x32 0x33 0x03 0x34 0x35 0x36 0xC8'")
    {
      message_sendSaveData(otaMessage, "Cmd", "123", 3);
      message_sendSaveData(otaMessage, "Data", "456", 3);
      message_sendIdleHandler(otaMessage);
      // printf("\r\n");

      THEN("send state be set false")
      {
        bool result = otaMessage->send.state;
        CHECK(result == false);
      }
    }

    WHEN("idle process response 'TRUE':'0x5A 0xA5 0x03 0x31 0x32 0x33 0x04 0x54 0x52 0x55 0x45 0xFB'")
    {
      message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");
      message_receiveHandler(otaMessage, "\x5A\xA5\x03\x31\x32\x33\x03\x34\x35\x36\xC8", 11);
      message_sendIdleHandler(otaMessage);

      THEN("send state be set false")
      {
        bool result = otaMessage->send.state;
        CHECK(result == false);
      }
    }

    WHEN("idle process response 'FALSE':'0x5A 0xA5 0x03 0x31 0x32 0x33 0x05 0x46 0x41 0x4C 0x53 0x45 0xA9'")
    {
      message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");
      message_receiveHandler(otaMessage, "\x5A\xA5\x03\x31\x32\x33\x03\x34\x35\x36\xC9", 11);
      message_sendIdleHandler(otaMessage);

      THEN("send state be set false")
      {
        bool result = otaMessage->send.state;
        CHECK(result == false);
      }
    }

    WHEN("send data & idle process")
    {
      message_sendSaveData(otaMessage, "Cmd", "123", 3);
      message_sendSaveData(otaMessage, "Data", "456", 3);
      message_sendIdleHandler(otaMessage);

      THEN("get data is '456'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetSendData(specifyComponent);
        CHECK_THAT(result, Equals("456"));
      }
    }

    WHEN("send two pieces of data & idle process")
    {
      message_sendSaveData(otaMessage, "Cmd", "123", 3);
      message_sendSaveData(otaMessage, "Data", "456", 3);
      message_sendIdleHandler(otaMessage);

      message_sendSaveData(otaMessage, "Cmd", "234", 3);
      message_sendSaveData(otaMessage, "Data", "567", 3);
      message_sendIdleHandler(otaMessage);

      THEN("get data is '567'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetSendData(specifyComponent);
        CHECK_THAT(result, Equals("567"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("get receive data & length after response")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");
  message_sendSetCallBack(otaMessage, NULL, message_callBack);

  GIVEN("register components")
  {
    message_bindingHead(otaMessage, "Head", "\x5A\xA5", 2);
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_bindingVariableData(otaMessage, "Data", 1);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");

    message_registerProcess(otaMessage, "123", "Data", NULL);

    WHEN("receive data & response 'TRUE'")
    {
      message_receiveHandler(otaMessage, "\x5A\xA5\x03\x31\x32\x33\x03\x34\x35\x36\xC8", 11);
      message_idleHandler(otaMessage);

      THEN("get cmd is '123'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        result = (char *)message_componentGetReceiveData(specifyComponent);
        CHECK_THAT(result, Equals("123"));
      }

      THEN("get cmd length 3")
      {
        int result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        result = message_componentGetReceiveDataLength(specifyComponent);
        CHECK(result == 3);
      }

      THEN("get data is '456'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetReceiveData(specifyComponent);
        CHECK_THAT(result, Equals("456"));
      }

      THEN("get data length is 3")
      {
        int result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = message_componentGetReceiveDataLength(specifyComponent);
        CHECK(result == 3);
      }

      THEN("get response cmd is '123'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        result = (char *)message_componentGetSendData(specifyComponent);
        CHECK_THAT(result, Equals("123"));
      }

      THEN("get response cmd length 3")
      {
        int result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Cmd");
        result = message_componentGetSendDataLength(specifyComponent);
        CHECK(result == 3);
      }

      THEN("get response data is 'TRUE'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetSendData(specifyComponent);
        CHECK_THAT(result, Equals("TRUE"));
      }

      THEN("get response data length is 4")
      {
        int result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = message_componentGetSendDataLength(specifyComponent);
        CHECK(result == 4);
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("get crc after receive multiple data")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");
  message_sendSetCallBack(otaMessage, NULL, message_callBack);

  GIVEN("register components")
  {
    message_bindingHead(otaMessage, "Head", "\x5A\xA5", 2);
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_bindingVariableData(otaMessage, "Data", 2);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    WHEN("receive multiple data")
    {
      message_receiveHandler(otaMessage, "\x5A\xA5", 2);
      message_receiveHandler(otaMessage, "\x05", 1);
      message_receiveHandler(otaMessage, "\x73\x6c\x69\x63\x65", 5);
      message_receiveHandler(otaMessage, "\x00\x41", 2);
      message_receiveHandler(otaMessage, "\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x31\x31\x31\x31\x31\x31\x31\x31\x31\x31\x32\x32\x32\x32\x32\x32\x32\x32\x32\x32\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x34\x34\x34\x34\x34\x34\x34\x34\x34\x34\x35\x35\x35\x35\x35\x35\x35\x35\x35\x35\x36\x36\x36\x36", 64);
      message_receiveHandler(otaMessage, "\x36", 1);
      message_receiveHandler(otaMessage, "\xEA", 1);

      THEN("get crc8 is '0xEA'")
      {
        unsigned char result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        result = message_componentGetReceiveData(specifyComponent)[0];
        CHECK(result == 0xEA);
      }
    }

    WHEN("receive data & response 'TRUE'")
    {
      message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");
      message_registerProcess(otaMessage, "123", "Data", NULL);

      message_receiveHandler(otaMessage, "\x5A\xA5\x03\x31\x32\x33\x00\x03\x34\x35\x36\x97", 12);
      message_sendIdleHandler(otaMessage);

      THEN("get response data is 'TRUE'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetSendData(specifyComponent);
        CHECK_THAT(result, Equals("TRUE"));
      }
    }
  }

  message_moduleFree(otaMessage);
}

SCENARIO("get tail after receive multiple data")
{
  messageModule *otaMessage = (messageModule *)message_moduleNew("ota");
  message_sendSetCallBack(otaMessage, NULL, message_callBack);

  GIVEN("register components")
  {
    message_bindingHead(otaMessage, "Head", "\x5A\xA5", 2);
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_bindingVariableData(otaMessage, "Data", 2);
    message_bindingTail(otaMessage, "Tail", "\x96\x69", 2);

    WHEN("receive multiple data")
    {
      message_receiveHandler(otaMessage, "\x5A\xA5", 2);
      message_receiveHandler(otaMessage, "\x05", 1);
      message_receiveHandler(otaMessage, "\x73\x6c\x69\x63\x65", 5);
      message_receiveHandler(otaMessage, "\x00\x41", 2);
      message_receiveHandler(otaMessage, "\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x31\x31\x31\x31\x31\x31\x31\x31\x31\x31\x32\x32\x32\x32\x32\x32\x32\x32\x32\x32\x33\x33\x33\x33\x33\x33\x33\x33\x33\x33\x34\x34\x34\x34\x34\x34\x34\x34\x34\x34\x35\x35\x35\x35\x35\x35\x35\x35\x35\x35\x36\x36\x36\x36", 64);
      message_receiveHandler(otaMessage, "\x36", 1);
      message_receiveHandler(otaMessage, "\x96\x69", 2);

      THEN("get Tail is '0x96 0x69'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Tail");
        result = (char *)message_componentGetReceiveData(specifyComponent);
        CHECK_THAT(result, Equals("\x96\x69"));
      }
    }

    WHEN("receive data & response 'TRUE'")
    {
      message_sendSetResponse(otaMessage, "Data", "TRUE", "FALSE");
      message_registerProcess(otaMessage, "123", "Data", NULL);

      message_receiveHandler(otaMessage, "\x5A\xA5\x03\x31\x32\x33\x00\x03\x34\x35\x36\x96\x69", 13);
      message_sendIdleHandler(otaMessage);

      THEN("get response data is 'TRUE'")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "Data");
        result = (char *)message_componentGetSendData(specifyComponent);
        CHECK_THAT(result, Equals("TRUE"));
      }
    }
  }

  message_moduleFree(otaMessage);
}