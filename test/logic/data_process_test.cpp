#include <string.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "logic/data_process.h"
#include "mock/math_mock.hpp"
#include "common/custom_utility.h"

using Catch::Matchers::Equals;

SCENARIO("compare two array")
{
    GIVEN("a array is greater than b array")
    {
        uint8_t a[4] = {0x00, 0x00, 0x00, 0x60};
        uint8_t b[4] = {0x00, 0x00, 0x00, 0x50};
        int result = 0;

        WHEN("call compare function")
        {
            result = Value4ByteComp_le(a, b);

            THEN("get result is equal to 1")
            {
                CHECK(result == 1);
            }
        }
    }

    GIVEN("a array is less than b array")
    {
        uint8_t a[4] = {0x00, 0x00, 0x00, 0x40};
        uint8_t b[4] = {0x00, 0x00, 0x00, 0x50};
        int result = 0;

        WHEN("call compare function")
        {
            result = Value4ByteComp_le(a, b);

            THEN("get result is equal to -1")
            {
                CHECK(result == -1);
            }
        }
    }
}

SCENARIO("test stub")
{
    GIVEN("Specify stub return 10")
    {
        int result = 0;
        ALLOW_CALL(stubMath, numberDouble(_)).RETURN(10);

        WHEN("call decrease() include stub function")
        {
            result = decrease();

            THEN("get result that stub return value decrease 1")
            {
                CHECK(result == 9);
            }
        }
    }
}

SCENARIO("use dictionary to get value")
{
    GIVEN("generate a data,and key is 'test2='")
    {
        uint8_t data[] = {"test=1234\r\n test2=abcdef\r\n test3=12abc\r\n"};
        uint8_t key[] = "test2=";
        uint8_t value[10] = {'\0'};
        std::string result;

        WHEN("get value")
        {
            uint32_t valueLength = 0;

            dictionary(data, key, value, &valueLength);

            THEN("value is equal to 'abcdef',and length is 6")
            {
                result = (char *)value;
                CHECK(valueLength == 6);
                CHECK_THAT(result, Equals("abcdef"));
            }
        }
    }
}
