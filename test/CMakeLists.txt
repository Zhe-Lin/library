set(BINARY ${CMAKE_PROJECT_NAME}_test)

set(HEADERS

)

set(SOURCES
    main/main_test.cpp
    logic/data_process_test.cpp
    # file/file_access_test.cpp
    # file/file_settings_test.cpp
    # file/file_process_test.cpp
    time/time_process_test.cpp
    mock/math_mock.cpp
    mock/time_mock.cpp
    encrypt/encrypt_test.cpp
    transform/transform_ascii_test.cpp

    # message/message_cJSON_test.cpp
    # message/message_application_test.cpp
    # message/message_factory_test.cpp
    # message/message_head_test.cpp
    # message/message_check_test.cpp
    # message/message_record_test.cpp

    message2/message_component_test.cpp
    message2/message_test.cpp
    message2/message_head_test.cpp
    message2/message_command_test.cpp
    message2/message_data_test.cpp
    message2/message_crc_test.cpp
    message2/message_tail_test.cpp
    
    # ../source/common/math.c
)

add_executable(${BINARY} ${SOURCES})

add_test(NAME ${BINARY} COMMAND ${BINARY})

find_package(Catch2 REQUIRED)
find_package(trompeloeil REQUIRED)
target_link_libraries(${BINARY} PUBLIC ${CMAKE_PROJECT_NAME}_lib Catch2::Catch2 trompeloeil::trompeloeil gcov)

